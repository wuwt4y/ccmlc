<?php
function my_errcode($code){
	switch ($code) {
		case '400':

			break;
		case '420':
			return _('420 帳號不存在!!');
			break;
		case '421':
			return _('421 密碼錯誤!!');
			break;
		case '450':
			return _('450 帳號格式錯誤,只能輸入英文數字底線!!');
			break;
		case '451':
			return _('451 帳號長度錯誤,限制6-25碼!!');
			break;
		case '452':
			return _('452 密碼格式錯誤,只能輸入英文數字底線!!');
			break;
		case '453':
			return _('453 密碼長度錯誤,限制6-25碼!!');
			break;
		case '454':
			return _('454 密碼不能與帳號相同!!');
			break;
		case '455':
			return _('455 二次密碼不相符!!');
			break;
		case '460':
			return _('460 WhatsApp帳號格式錯誤!!');
			break;
		case '461':
			return _('461 Skype帳號格式錯誤!!');
			break;
		case '462':
			return _('462 Line帳號格式錯誤!!');
			break;
		case '463':
			return _('463 電話號碼格式錯誤!!');
			break;
		case '470':
			return _('470 E-mail格式錯誤!!');
			break;
		default:
			# code...
			break;
	}
}
function err_log($error_line,$error_code,$error_message) {
	$err_sql = "INSERT INTO error_log(err_date,error_line,error_code,error_message,url,session_id,remote_ip) VALUES(:err_date,:error_line,:error_code,:error_message,:url,:session_id,:remote_ip)";
	$pdo = MyPDO::getInstance();
	$serr = $pdo->prepare($err_sql);
	$serr->bindValue(':err_date',date("YmdHis"));
	$serr->bindValue(':error_line',$error_line);
	$serr->bindValue(':error_code',$error_code);
	$serr->bindValue(':error_message',$error_message);
	$serr->bindValue(':url',$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	$serr->bindValue(':session_id',$_SESSION['usr_id']);
	$serr->bindValue(':remote_ip',$_SERVER['REMOTE_ADDR']);
	$serr->execute();
}

function is_Date($input_date){
    //正則
    $reg_one = "/^(\d{3,4})\-(\d{1,2})\-(\d{1,2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/";
    $reg_two = "/^(\d{3,4})\-(\d{1,2})\-(\d{1,2})$/";

    //檢查格式後用php function做日期檢查
    if (preg_match($reg_one, $input_date, $matches) || preg_match($reg_two, $input_date, $matches))
    {
        if (checkdate($matches[2], $matches[3], $matches[1])) {
            return '1';
        }else{
        	return '0';
        }
    }
    return '0';
}

function is_Time($input_time){
	$reg = "/^([01][0-9]|2[0-3]):([0-5][0-9])$/";
	if (preg_match($reg,$input_time)) {
		return true;
	}else{
		return false;
	}
}

function is_Id($input_id){
	// $reg = "/^[a-zA-Z0-9_]+$";
	if (preg_match('/^[a-zA-Z0-9_]+$/',$input_id)) {
		return true;
	}else{
		return false;
	}
}


?>