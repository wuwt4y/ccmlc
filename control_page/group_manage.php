<?php

?>
<script type="text/javascript">
	$(function() {
		gtable = $('#Table_group').DataTable( {
	        "oLanguage": {"sUrl": "../js/zh_TW.txt"},
	        // "dom": 'Zlfrtip',
	        // "colResize": {
	        // "tableWidthFixed": false
	        // },
	        "processing": true,
	        "serverSide": true,
	        "ajax": {
	            "url": "../processing/group_manage_processing.php?mode=list",
	            "type": "POST"
	        },
	        // "columnDefs": [
	        //   {"targets":  '_all',"orderable":true,"searchable": false,"visible": true},
	        // ],
	        "columns":
	         [
	            { "data": "group_id","width": "10%" },
	            { "data": "group_name","width": "15%" },
	            { "data": "c_date","width": "15%" },
	            { "data": "c_id" ,"width": "5%"},
	            { "data": "0" ,"width": "2%","orderable":false,"searchable":false},
	            { "data": "1" ,"width": "2%","orderable":false,"searchable":false}
	        ],
	        "order": [[0, 'desc']]
    	} );
        $('#save_group').click(function(){
                $.ajax({
                    url: '../processing/group_manage_processing.php?mode=add',
                    type: 'POST',
                    dataType: 'html',
                    data: {data: $('#a_group_name').val()},
                    cache: false
                })
                .done(function(e) {
                    gtable.ajax.reload();
                    $.fancybox.close('#add_group_box');
                    console.log(e);
                })
                .fail(function() {
                    console.log("error");
                });
            });
    	$('#Table_group').on('click','#GroupDel',function(){
    		var group_id = $(this).val();
    		$.ajax({
    			url: '../processing/group_manage_processing.php?mode=del',
    			type: 'POST',
    			dataType: 'html',
    			data: {group_id: group_id},
    		})
    		.done(function() {
    			gtable.ajax.reload();
    		})
    		.fail(function() {
    			console.log("error");
    		});

    	});
    	$('#Table_group').on('click','#GroupUpdate',function(){
    		var group_id = $(this).val();
    		var group_name = $(this).parent().siblings().eq(1).text();
    		$('#hide_g_trn').val(group_id);
    		$('#e_group_name').val(group_name);
    		$.fancybox.open({
					src:'#edit_group_box',
					type:'inline',
					opts : {
						afterClose: function( instance, current ) {
							$('#e_group_name').val('');
							$('#hide_g_trn').val('');
						},
		                afterShow : function( instance, current ) {
		                // console.info( instance );
		                }
           			 }
				});
    	});
    	$('body').on('click','#esave_group',function(){
    		var group_id = $('#hide_g_trn').val();
    		var group_name = $('#e_group_name').val();
    		$.ajax({
    			url: '../processing/group_manage_processing.php?mode=edit',
    			type: 'POST',
    			dataType: 'html',
    			data: {group_id: group_id,group_name: group_name},
    		})
    		.done(function() {
    			gtable.ajax.reload();
    			$.fancybox.close('#edit_group_box');
    		})
    		.fail(function() {
    			console.log("error");
    		});
    	});
	});
</script>
<button id="add_group" class="btn btn-warning"><?=_('新增群組')?></button>
<div id="add_group_box">
	<?=_('群組名稱')?>：
	<input type="text" name="a_group_name" id="a_group_name">
	<button id="save_group"><?=_('儲存')?></button>
</div>
<div class="sdatatabble">
  <table id="Table_group" class="display" cellspacing="0">
        <thead>
            <tr>
              <th><?=_('群組編號')?></th>
              <th><?=_('群組名稱')?></th>
              <th><?=_('群組建立時間')?></th>
              <th><?=_('群組建立人')?></th>
              <th><?=_('設定')?></th>
              <th><?=_('刪除')?></th>
            </tr>
        </thead>
    </table>
</div>
<div id="edit_group_box">
	<?=_('群組名稱')?>：
	<input type="text" name="e_group_name" id="e_group_name">
	<input type="hidden" name="hide_g_trn" id="hide_g_trn">
	<button id="esave_group"><?=_('儲存')?></button>
</div>
