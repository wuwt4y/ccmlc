<?php

?>
<script type="text/javascript">
	$(function() {
		$( "#group_permission" ).selectmenu();

		$( "#tabs" ).tabs();
		$.ajax({
			url: '../processing/group_manage_processing.php?mode=select_group_list',
			type: 'POST',
			dataType: 'json'
		})
		.done(function(e) {
			for (var i = 0; i < e.length; i++) {
				 $("#group_permission").append($("<option></option>").attr("value", e[i].group_id).text(e[i].group_name));
			}
		})
		.fail(function() {
			console.log("error");
		});
	});
</script>
<style type="text/css">

</style>
<div class="permission_head">
	<?=_('選擇群組：')?>
	<select name="group_permission" id="group_permission" onchange="onSelectPermissionGroup();">
		<option></option>
	</select>
</div>
<div class="permission_content">
	<div class="permission_title">
	</div>


</div>

<div id="tabs">
  <ul>
    <li><a href="../control_page/resume_permission.php"><?=_('履歷表權限設定')?></a></li>
    <li><a href="../control_page/form_flow.php"><?=_('表單流程權限設定')?></a></li>
    <li><a href="../control_page/admin_permission.php"><?=_('系統管理員')?></a></li>
  </ul>

</div>