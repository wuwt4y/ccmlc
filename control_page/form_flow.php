<script type="text/javascript">
	$(function() {
		$.ajax({
			url: '../processing/resume_permission_processing.php?mode=list',
			type: 'POST',
			dataType: 'json'
		})
		.done(function(e) {
			for (var i = 0; i < e.length; i++) {
				$('#form_flow_l').append('<div id="page_flow_l_'+e[i]+'">'+e[i]+'</div>');
			}
		})
		.fail(function() {
			console.log("error");
		});

		$('body').on('click','#form_to_right',function(){
			var json_selected = $('#form_flow_l div[selected="selected"]').map(function(index, elem) {
				return $( this ).text();
			});
			console.log($('div[id^="page_flow_r_"]').map(function(index, elem) {
				return $( this ).text();
			}));
			for (var i = 0; i < json_selected.length; i++) {
				if ($('div[id^="page_flow_r_"]').text().indexOf(json_selected[i])!==-1) {
					continue;
				}
				$('#form_flow_r').append('<div id="page_flow_r_'+json_selected[i]+'">'+json_selected[i]+'</div>');
			}
		});
		$('body').on('click','#form_to_left',function(){
			$('#form_flow_r div[selected="selected"]').remove();
		});
	});
</script>
<div class="form_flow">
	<div id="form_flow_l">
	</div>
	<div id="form_flow_m">
		<div>
			<button id="form_to_right"><img src="../images/next.png"></button>
		</div>
		<div>
			<button id="form_to_left"><img src="../images/back.png"></button>
		</div>
	</div>
	<div id="form_flow_r">

	</div>
	表單流程名稱：
<input type="text" name="form_name" id="form_name">
<button name="save_formname" id="save_formname" class="btn btn-sm btn-info"><?=_('儲存')?></button>
</div>
