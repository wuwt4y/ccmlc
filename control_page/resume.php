<script type="text/javascript">
	$(function() {
	    var $Table_resume = $("#Table_resume");
	    var $TableResumeMes = $('#TableResumeMes');
		var resume_table = $Table_resume.DataTable( {
	        "oLanguage": {"sUrl": "../js/zh_TW.txt"},
	        // "dom": 'Zlfrtip',
	        // "colResize": {
	        // "tableWidthFixed": false
	        // },
	        "processing": true,
	        "serverSide": true,
	        "ajax": {
	            "url": "../processing/resume_processing.php?mode=list",
	            "type": "POST"
	        },
	        // "columnDefs": [
	        //   {"targets":  '_all',"orderable":true,"searchable": false,"visible": true},
	        // ],
	        "columns":
	         [
	            { "data": "resume_id","width": "10%" },
	            { "data": "r_name","width": "15%" },
	            { "data": "birthday","width": "15%" },
	            { "data": "c_date" ,"width": "2%"},
	            { "data": "c_id" ,"width": "2%"},
	            { "data": "0" ,"width": "2%","orderable":false,"searchable":false},
	            { "data": "1" ,"width": "2%","orderable":false,"searchable":false},
	            { "data": "2" ,"width": "2%","orderable":false,"searchable":false}

	        ],
	        "order": [[0, 'desc']]
    	} );
		var TableResumeMes = $TableResumeMes.DataTable( {
	        "oLanguage": {"sUrl": "../js/zh_TW.txt"},
	        // "dom": 'Zlfrtip',
	        // "colResize": {
	        // "tableWidthFixed": false
	        // },
	        "processing": true,
	        "serverSide": true,
	        "ajax": {
	            "url": "../processing/resume_processing.php?mode=meslist",
	            "type": "POST"
	        },
	        // "columnDefs": [
	        //   {"targets":  '_all',"orderable":true,"searchable": false,"visible": true},
	        // ],
	        "columns":
	         [
	            { "data": "r_name","width": "10%" },
	            { "data": "birthday","width": "15%" },
	            { "data": "introname","width": "15%" },
	            { "data": "introcontact" ,"width": "2%"},
	            { "data": "c_date" ,"width": "2%"},
	            { "data": "0" ,"width": "2%","orderable":false,"searchable":false},
	            { "data": "1" ,"width": "2%","orderable":false,"searchable":false}
	        ],
	        "order": [[0, 'desc']]
    	} );

        $Table_resume.on('change', '[id^="foot_"]', function () {
		// console.log(this.id);
		// console.log(this.value);
		var coidx = this.id.replace('foot_','');
		resume_table.column( coidx ).search( this.value ).draw();
        } );
        $TableResumeMes.on('change', '[id^="foot_"]', function () {
		// console.log(this.id);
		// console.log(this.value);
		var coidx = this.id.replace('foot_','');
		TableResumeMes.column( coidx ).search( this.value ).draw();
        } );
        $Table_resume.on('click','#sDel',function(){
	    	var resume_id = $(this).val();
	    	// console.log(resume_id);
	    	$.ajax({
	    		url: '../processing/resume_processing.php?mode=del',
	    		type: 'POST',
	    		dataType: 'json',
	    		data: {resume_id: resume_id},
	    	})
	    	.done(function(e) {
	    		// console.log(e);
	    		if (e == 'success') {resume_table.ajax.reload();}
	    	})
	    	.fail(function() {
	    		console.log("error");
	    		});
	    	});
        $TableResumeMes.on('click','#sDelmes',function(){
	    	var trn = $(this).val();
	    	// console.log(resume_id);
	    	$.ajax({
	    		url: '../processing/resume_processing.php?mode=mesdel',
	    		type: 'POST',
	    		dataType: 'json',
	    		data: {trn: trn}
	    	})
	    	.done(function(e) {
	    		// console.log(e);
	    		if (e == 'success') {TableResumeMes.ajax.reload();}
	    	})
	    	.fail(function() {
	    		console.log("error");
	    		});
	    	});
        $Table_resume.on('click','#sView',function(){
            var randNumber = Math.floor(Math.random()*99);
            var resume_id = $(this).val();
	    	window.open('../pre.php'+'?con='+randNumber+'#'+resume_id, '履歷表', config='height=500,width=500,toolbar=no');
	    });
        $TableResumeMes.on('click','#sViewmes',function(){

        });
	});
</script>
<style type="text/css">
	#Table_resume thead{
		background-color: hsl(162, 36%, 53%);
	}
</style>
<button id="add_resume_bu" class="btn btn-warning"><?=_('新增履歷')?></button>
<button id="ViewResumeMessageBu" class="btn btn-warning"><?=_('簡易履歷')?></button>

<div class="rdatatabble">
  <table id="Table_resume" class="display" cellspacing="0">
        <thead>
            <tr>
              <th><?=_('編號')?></th>
              <th><?=_('姓名')?></th>
              <th><?=_('生日')?></th>
              <th><?=_('建立時間')?></th>
              <th><?=_('建立人')?></th>
              <th><?=_('瀏覽')?></th>
              <th><?=_('設定')?></th>
              <th><?=_('刪除')?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th><label for="foot_0"></label><input type="text" id="foot_0"></th>
                <th><label for="foot_1"></label><input type="text" id="foot_1"></th>
                <th><label for="foot_2"></label><input type="text" id="foot_2"></th>
                <th><label for="foot_3"></label><input type="text" id="foot_3"></th>
                <th><label for="foot_4"></label><input type="text" id="foot_4"></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>

<div class="rdatatabble">
  <table id="TableResumeMes" class="display" cellspacing="0">
        <thead>
            <tr>
              <th><?=_('勞工姓名')?></th>
              <th><?=_('勞工聯絡方式')?></th>
              <th><?=_('介紹人姓名')?></th>
              <th><?=_('介紹人聯絡方式')?></th>
              <th><?=_('上傳時間')?></th>
              <th><?=_('帶入')?></th>
              <th><?=_('刪除')?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th><label for="foot_0"></label><input type="text" id="foot_0"></th>
                <th><label for="foot_1"></label><input type="text" id="foot_1"></th>
                <th><label for="foot_2"></label><input type="text" id="foot_2"></th>
                <th><label for="foot_3"></label><input type="text" id="foot_3"></th>
                <th><label for="foot_4"></label><input type="text" id="foot_4"></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>