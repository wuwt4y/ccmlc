<?php

?>
<script type="text/javascript">
	$(document).ready(function() {
		table = $('#Table_staff').DataTable( {
	        "oLanguage": {"sUrl": "../js/zh_TW.txt"},
	        // "dom": 'Zlfrtip',
	        // "colResize": {
	        // "tableWidthFixed": false
	        // },
	        "processing": true,
	        "serverSide": true,
	        "ajax": {
	            "url": "../processing/staff_processing.php?mode=list",
	            "type": "POST"
	        },
	        // "columnDefs": [
	        //   {"targets":  '_all',"orderable":true,"searchable": false,"visible": true},
	        // ],
	        "columns":
	         [
	            { "data": "usr_name","width": "10%" },
	            { "data": "usr_id","width": "15%" },
	            { "data": "usr_email","width": "15%" },
	            { "data": "usr_tel" ,"width": "5%"},
	            { "data": "usr_whatsapp" ,"width": "5%"},
	            { "data": "usr_skype" ,"width": "5%"},
	            { "data": "usr_line" ,"width": "2%"},
	            { "data": "group_name" ,"width": "2%"},
	            { "data": "0" ,"width": "2%","orderable":false,"searchable":false},
	            { "data": "1" ,"width": "2%","orderable":false,"searchable":false}

	        ],
	        "order": [[0, 'desc']]
    	} );
    $('body').on('change', '[id^="foot_"]', function () {
		var table = $('#Table_staff').DataTable();
		// console.log(this.id);
		// console.log(this.value);
		var coidx = this.id.replace('foot_','');
		table.column( coidx ).search( this.value ).draw();
        } );
    $('#Table_staff').on('click','#sDel',function(){
    	var trn_no = $(this).val();
    	console.log(trn_no);
    	$.ajax({
    		url: '../processing/staff_processing.php?mode=del',
    		type: 'POST',
    		dataType: 'html',
    		data: {trn_no: trn_no},
    	})
    	.done(function(e) {
    		// console.log(e);
    		table.ajax.reload();
    	})
    	.fail(function() {
    		console.log("error");
    		});
    	});
    $('#Table_staff').on('click','#sUpdate',function(){
    	$.fancybox.open({
    		src:'#edit_staff_box',
			type:'inline',
			opts : {
		        afterShow : function( instance, current ) {
		        // console.info( 'done!' );
		        }
           	}
    	});
    	var trn_no = $(this).val();
    	console.log(trn_no);
    	$.ajax({
    		url: '../processing/staff_processing.php?mode=editlist',
    		type: 'POST',
    		dataType: 'json',
    		data: {trn_no: trn_no},
    	})
    	.done(function(e) {
    		// console.log(e);
    		$('#eusr_name').val(e[0].usr_name);
    		$('#eusr_email').val(e[0].usr_email);
    		$('#eusr_whatsapp').val(e[0].usr_whatsapp);
    		$('#eusr_skype').val(e[0].usr_skype);
    		$('#eusr_line').val(e[0].usr_line);
    		$('#eusr_tel').val(e[0].usr_tel);
    		$('#eselect_group').val(e[0].group_id);
    		$('#edit_trn').val(trn_no);
    	})
    	.fail(function() {
    		console.log("error");
    		});
    	});
    $('body').on('click','#esave_staff',function(){
    	$.ajax({
    		url: '../processing/staff_processing.php?mode=edit',
    		type: 'POST',
    		dataType: 'html',
    		data: {data: $('#edit_staff_box input,select[name="egroup_id"]').serialize()},
    	})
    	.done(function(e) {
    		console.log(e);
    		table.ajax.reload();
    		$('#err_edit_staff').text(e).show();
    		$.fancybox.close('#edit_staff_box');
    	})
    	.fail(function() {
    		console.log("error");
    		});
    	});
	});
</script>
<div id="staff_manage">
<button class="btn btn-warning" id="add_staff_bu"><?=_('新增員工')?></button>
<div id="add_staff_box" name="add_staff_box">

	<div class="add_staff_inline">
		<?=_('姓名：')?><input type="text" name="usr_name" id="usr_name">
	</div>
	<div class="add_staff_inline">
		<?=_('帳號：')?><input type="text" name="usr_id" id="usr_id">
	</div>
	<div class="add_staff_inline">
		<?=_('密碼：')?><input type="password" name="usr_passwd" id="usr_passwd">
	</div>
	<div class="add_staff_inline">
		<?=_('再一次密碼：')?><input type="password" name="usr_passwd2" id="usr_passwd2">
	</div>
	<div class="add_staff_inline">
		<?=_('E-Mail：')?><input type="text" name="usr_email" id="usr_email">
	</div>
	<div class="add_staff_inline">
		<?=_('WhatsApp：')?><input type="text" name="usr_whatsapp" id="usr_whatsapp">
	</div>
	<div class="add_staff_inline">
		<?=_('Skype：')?><input type="text" name="usr_skype" id="usr_skype">
	</div>
	<div class="add_staff_inline">
		<?=_('Line：')?><input type="text" name="usr_line" id="usr_line">
	</div>
	<div class="add_staff_inline">
		<?=_('電話：')?><input type="text" name="usr_tel" id="usr_tel">
	</div>
	<div class="add_staff_inline">
		<?=_('群組：')?>
		<select id="select_group" name="group_id" class="styled-select" onchange = "onSelectGroup()" ></select>
	</div>
	<div class="add_staff_inline">
		<button class="btn btn-sm btn-info" id="save_staff"><?=_('儲存')?></button>
	</div>
	<div id="err_add_staff" class="alert alert-warning" role="alert"></div>
</div>

<div class="sdatatabble">
  <table id="Table_staff" class="display" cellspacing="0">
        <thead>
            <tr>
              <th><?=_('姓名')?></th>
              <th><?=_('帳號')?></th>
              <th><?=_('E-Mail')?></th>
              <th><?=_('電話')?></th>
              <th><?=_('WhatsApp')?></th>
              <th><?=_('Skype')?></th>
              <th><?=_('Line')?></th>
              <th><?=_('群組')?></th>
              <th><?=_('設定')?></th>
              <th><?=_('刪除')?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th><input type="text" id="foot_0"></th>
                <th><input type="text" id="foot_1"></th>
                <th><input type="text" id="foot_2"></th>
                <th><input type="text" id="foot_3"></th>
                <th><input type="text" id="foot_4"></th>
                <th><input type="text" id="foot_5"></th>
                <th><input type="text" id="foot_6"></th>
                <th><input type="text" id="foot_7"></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>

<div id="edit_staff_box" name="edit_staff_box">
	<div class="edit_staff_inline">
		<?=_('姓名：')?><input type="text" name="usr_name" id="eusr_name">
	</div>
	<div class="edit_staff_inline">
		<?=_('E-Mail：')?><input type="text" name="usr_email" id="eusr_email">
	</div>
	<div class="edit_staff_inline">
		<?=_('WhatsApp：')?><input type="text" name="usr_whatsapp" id="eusr_whatsapp">
	</div>
	<div class="edit_staff_inline">
		<?=_('Skype：')?><input type="text" name="usr_skype" id="eusr_skype">
	</div>
	<div class="edit_staff_inline">
		<?=_('Line：')?><input type="text" name="usr_line" id="eusr_line">
	</div>
	<div class="edit_staff_inline">
		<?=_('電話：')?><input type="text" name="usr_tel" id="eusr_tel">
	</div>
	<div class="edit_staff_inline">
		<?=_('群組：')?>
		<select id="eselect_group" name="egroup_id" class="styled-select"></select>
	</div>
	<input type="hidden" name="edit_trn" id="edit_trn">
	<div class="edit_staff_inline">
		<button class="btn btn-sm btn-info" id="esave_staff"><?=_('儲存')?></button>
	</div>
	<div id="err_edit_staff" class="alert alert-warning" role="alert"></div>
</div>
</div>