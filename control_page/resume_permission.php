<script type="text/javascript">
	$(function() {
		$.ajax({
			url: '../processing/resume_permission_processing.php?mode=list',
			type: 'POST',
			dataType: 'json'
		})
		.done(function(e) {
			for (var i = 0; i < e.length; i++) {
				$('#tabs-2').append('<div id="resume_'+e[i]+'"><div class="resume_permission">'+e[i]+'</div><label for="resume_r_'+e[i]+'">讀取</label><input type="checkbox" name="'+e[i]+'[]" id="resume_r_'+e[i]+'" value="4"><label for="resume_w_'+e[i]+'">寫入</label><input type="checkbox" name="'+e[i]+'[]" id="resume_w_'+e[i]+'" value="2"><label for="resume_d_'+e[i]+'">刪除</label><input type="checkbox" name="'+e[i]+'[]" id="resume_d_'+e[i]+'" value="1"></div>');
			}
			// console.log(e);
			$( "input[type='checkbox']" ).checkboxradio();
			// $("#selectR").attr("checked","checked").change();
		})
		.fail(function() {
			console.log("error");
		});
		$('body').on('change','#selectAll',function(){
			if ($('#selectAll').prop('checked') == true) {
				$('div[id^="resume_"] input[type="checkbox"]').prop("checked","checked").change();
			}else{
				$('div[id^="resume_"] input[type="checkbox"]').prop("checked","").change();
			}
		});
		$('body').on('change','#selectR',function(){
			if ($('#selectR').prop('checked') == true) {
				$('input[id^="resume_r_"]').prop("checked","checked").change();
			}else{
				$('input[id^="resume_r_"]').prop("checked","").change();
			}
		});
		$('body').on('change','#selectW',function(){
			if ($('#selectW').prop('checked') == true) {
				$('input[id^="resume_w_"]').prop("checked","checked").change();
			}else{
				$('input[id^="resume_w_"]').prop("checked","").change();
			}
		});
		$('body').on('change','#selectD',function(){
			if ($('#selectD').prop('checked') == true) {
				$('input[id^="resume_d_"]').prop("checked","checked").change();
			}else{
				$('input[id^="resume_d_"]').prop("checked","").change();
			}
		});
		$('body').on('click','#sava_per_resume_bu',function(){
			var length = $('div[id^="resume_"]').length;
			var data_a = $('#tabs-2 input').serialize();
			var group_id = $("#group_permission").val();
			$.ajax({
				url: '../processing/resume_permission_processing.php?mode=set',
				type: 'POST',
				dataType: 'html',
				data: {
					data_a: data_a,group_id: group_id,length: length
				},
			})
			.done(function(e) {
				console.log(e);
			})
			.fail(function() {
				console.log("error");
			});
		});
	});
</script>
<div id="tabs-1">
	<label for="selectAll"><?=_('全選')?></label>
			<input type="checkbox" name="selectAll" id="selectAll">
	<label for="selectR"><?=_('讀取全選')?></label>
			<input type="checkbox" name="selectR" id="selectR">
	<label for="selectW"><?=_('寫入全選')?></label>
			<input type="checkbox" name="selectW" id="selectW">
	<label for="selectD"><?=_('刪除全選')?></label>
			<input type="checkbox" name="selectD" id="selectD">
	<div id="tabs-2"></div>

</div>
<button name="sava_per_resume_bu" id="sava_per_resume_bu" class="btn btn-sm btn-info"><?=_('儲存')?></button>
