<script type="text/javascript">
	$(function() {
		TableHireMes = $('#TableHireMes').DataTable( {
	        "oLanguage": {"sUrl": "../js/zh_TW.txt"},
	        // "dom": 'Zlfrtip',
	        // "colResize": {
	        // "tableWidthFixed": false
	        // },
	        "processing": true,
	        "serverSide": true,
	        "ajax": {
	            "url": "../processing/recruitment_processing.php?mode=meslist",
	            "type": "POST"
	        },
	        // "columnDefs": [
	        //   {"targets":  '_all',"orderable":true,"searchable": false,"visible": true},
	        // ],
	        "columns":
	         [
	            { "data": "name","width": "10%" },
	            { "data": "contact_way","width": "15%" },
	            { "data": "introducer_name","width": "15%" },
	            { "data": "introducer_contact" ,"width": "2%"},
	            { "data": "c_date" ,"width": "2%"},
	            { "data": "0" ,"width": "2%","orderable":false,"searchable":false},
	            { "data": "1" ,"width": "2%","orderable":false,"searchable":false}
	        ],
	        "order": [[0, 'desc']]
    	} );
	});
</script>
<button id="add_hiretable_bu" class="btn btn-warning"><?=_('新增聘工表')?></button>
<button id="ViewHireTableMessageBu" class="btn btn-warning"><?=_('簡易聘工表')?></button>


<div class="rdatatabble">
  <table id="TableHireMes" class="display" cellspacing="0">
        <thead>
            <tr>
              <th><?=_('雇主姓名')?></th>
              <th><?=_('雇主聯絡方式')?></th>
              <th><?=_('介紹人姓名')?></th>
              <th><?=_('介紹人聯絡方式')?></th>
              <th><?=_('上傳時間')?></th>
              <th><?=_('帶入')?></th>
              <th><?=_('刪除')?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th><input type="text" id="foot_0"></th>
                <th><input type="text" id="foot_1"></th>
                <th><input type="text" id="foot_2"></th>
                <th><input type="text" id="foot_3"></th>
                <th><input type="text" id="foot_4"></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>