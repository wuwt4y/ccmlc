<?php
session_start();
include_once "../vendor/autoload.php";
include_once('../include/function.php');
date_default_timezone_set('Asia/Taipei');

use HaoCls\staff\staff;
use HaoCls\dao\MyPDO;
use HaoCls\dao\MyQuery;

$date = date("YmdHis");
$mode = $_GET['mode'];
$staff = new staff;
switch ($mode) {
	case 'add_staff':
		parse_str($_POST['data'], $data);
		echo $staff->add_staff(json_encode($data));
		break;
	case 'list':
		$draw = $_POST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
        $orderByColumnIndex  = $_POST['order'][0]['column'];// index of the sorting column (0 index based - i.e. 0 is the first record)
        $orderBy = $_POST['columns'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
        $orderType = $_POST['order'][0]['dir']; // ASC or DESC
        $start  = $_POST["start"];//Paging first record indicator.
        $length = $_POST['length'];//Number of records that the table can display in the current draw
        $pdo = MyPDO::getInstance();
        $iFilteredTotal = MyQuery::sqlCount("SELECT * FROM staff");

		if(!empty($_POST['search']['value'])){
            for($i=0 ; $i<count($_POST['columns']);$i++){
            $column = $_POST['columns'][$i]['data'];
            //we get the name of each column using its index from POST request
            $where[]="$column like '%".$_POST['search']['value']."%'";
            }
            $where = implode(" OR " , $where);
            // echo $where;exit;
            $sql="SELECT a.*,b.group_name FROM staff AS a LEFT JOIN group_mapping AS b ON a.group_id = b.group_id WHERE  a.swt = '0' AND ($where) ORDER BY $orderBy $orderType limit $start,$length";
            // echo $sql;exit;
            $sh=$pdo->prepare($sql);
            $sh->execute();
            $recordsFiltered = MyQuery::sqlCount("SELECT a.*,b.group_name FROM staff AS a LEFT JOIN group_mapping AS b ON a.group_id = b.group_id WHERE  a.swt = '0' AND ($where)");
            }else{
            	$foowhere = '';
            	for($i=0 ; $i<count($_POST['columns']); $i++){
            		if (empty($_POST['columns'][$i]['search']['value'])) {
            			continue;
            	}else{
            		$column = $_POST['columns'][$i]['data'];
            		$foowhere.= " AND $column LIKE '%".$_POST['columns'][$i]['search']['value']."%' ";
            		}
            	}

            	$sql="SELECT a.*,b.group_name FROM staff AS a LEFT JOIN group_mapping AS b ON a.group_id = b.group_id WHERE a.swt = '0' $foowhere ORDER BY $orderBy $orderType limit $start,$length";
            $sh=$pdo->prepare($sql);
            $sh->execute();
            // $recordsFiltered = $iFilteredTotal;
            $recordsFiltered = MyQuery::sqlCount("SELECT a.*,b.group_name FROM staff AS a LEFT JOIN group_mapping AS b ON a.group_id = b.group_id WHERE a.swt = '0' $foowhere");
            }
            $i=0;
            while($row = $sh->fetch(PDO::FETCH_ASSOC)){
            $sUpdate="<button type='button' id='sUpdate' name='sUpdate' value='{$row['trn_no']}' style='border:none;background:none;'><img src='../images/edit_hao.png'></button>";
        	$sDel="<button type='button' id='sDel' name='sDel' value='{$row['trn_no']}' style='border:none;background:none;'><img src='../images/del_hao.png'></button>";
            $datainfo[$i]=$row;
        	$datainfo[$i][]=$sUpdate;
        	$datainfo[$i][]=$sDel;
            $i++;
            }
            if (isset($datainfo)==''){
            $datainfo='';
            }
            $sOutput=array(
            "draw"   => intval($draw),
            "recordsTotal"    => $iFilteredTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $datainfo
            );
            echo json_encode($sOutput);
		break;
	case 'del':
			$trn_no = $_POST['trn_no'];
			$staff->del_staff($trn_no);
		break;
	case 'editlist':
		$trn_no = $_POST['trn_no'];
		$sh = $staff->sh_staff($trn_no);
		echo $sh;
		break;
	case 'edit':
		parse_str($_POST['data'], $data);
		$json_data = json_encode($data);
		echo $staff->edit_staff($json_data);
		break;
	default:
		# code...
		break;
}
?>