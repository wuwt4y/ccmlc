<?php
session_start();
include_once "../vendor/autoload.php";
include_once('../include/function.php');
date_default_timezone_set('Asia/Taipei');
$date = date("YmdHis");

use HaoCls\group\group;
use HaoCls\dao\MyPDO;
use HaoCls\dao\MyQuery;

$group = new group;
$mode = $_GET['mode'];
switch ($mode) {
	case 'add':
		$group_name = $_POST['data'];
		$group->add_group($group_name, $_SESSION['usr_id']);
		// echo $group_name;
		break;
	case 'select_group_list':
		$group_list = $group->select_group_list();
		echo $group_list;
		break;
	case 'list':
		$draw = $_POST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
        $orderByColumnIndex  = $_POST['order'][0]['column'];// index of the sorting column (0 index based - i.e. 0 is the first record)
        $orderBy = $_POST['columns'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
        $orderType = $_POST['order'][0]['dir']; // ASC or DESC
        $start  = $_POST["start"];//Paging first record indicator.
        $length = $_POST['length'];//Number of records that the table can display in the current draw
        $pdo = MyPDO::getInstance();
        $iFilteredTotal = MyQuery::sqlCount("SELECT * FROM group_mapping");

		if(!empty($_POST['search']['value'])){
            for($i=0 ; $i<count($_POST['columns']);$i++){
            $column = $_POST['columns'][$i]['data'];
            //we get the name of each column using its index from POST request
            $where[]="$column like '%".$_POST['search']['value']."%'";
            }
            $where = implode(" OR " , $where);
            // echo $where;exit;
            $sql="SELECT * FROM group_mapping WHERE  swt = '0' AND ($where) ORDER BY $orderBy $orderType limit $start,$length";

            $sh=$pdo->prepare($sql);
            $sh->execute();
            $recordsFiltered = MyQuery::sqlCount("SELECT * FROM group_mapping WHERE  swt = '0' AND ($where)");
            }else{
            $sql="SELECT * FROM group_mapping WHERE swt = '0' ORDER BY $orderBy $orderType limit $start,$length";
            $sh=$pdo->prepare($sql);
            $sh->execute();
            $recordsFiltered = $iFilteredTotal;

            }
            $i=0;
            while($row = $sh->fetch(PDO::FETCH_ASSOC)){
            $GroupUpdate="<button type='button' id='GroupUpdate' name='GroupUpdate' value='{$row['group_id']}' style='border:none;background:none;'><img src='../images/edit_hao.png'></button>";
        	$GroupDel="<button type='button' id='GroupDel' name='GroupDel' value='{$row['group_id']}' style='border:none;background:none;'><img src='../images/del_hao.png'></button>";
            $datainfo[$i]=$row;
        	$datainfo[$i][]=$GroupUpdate;
        	$datainfo[$i][]=$GroupDel;
            $i++;
            }
            if (isset($datainfo)==''){
            $datainfo='';
            }
            $sOutput=array(
            "draw"   => intval($draw),
            "recordsTotal"    => $iFilteredTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $datainfo
            );
            echo json_encode($sOutput);
		break;
	case 'del':
		$group_id = $_POST['group_id'];
		$group->del_group($group_id);
		break;
	case 'edit':
		$group_id = $_POST['group_id'];
		$group_name = $_POST['group_name'];
		$group->edit_group($group_id,$group_name);
		break;
	default:
		# code...
		break;
}
?>