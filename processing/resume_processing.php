<?php
session_start();
include_once('../include/function.php');
include_once '../vendor/autoload.php';
date_default_timezone_set('Asia/Taipei');

use HaoCls\datatable\findjob;
use HaoCls\resume\resume;
use HaoCls\datatable\datatable;
use HaoCls\upload\Upload;
use HaoCls\upload\UploadDAO;

$resume = new resume;

$mode = $_GET['mode'];

switch ($mode) {
	case 'list':
        $extendcolumn = array(
            "sView" => "<button type='button' id='sView' name='sView' value style='border:none;background:none;'><img src='../images/preview.png'></button>",
            "sUpdate" => "<button type='button' id='sUpdate' name='sUpdate' value style='border:none;background:none;'><img src='../images/edit_hao.png'></button>",
            "sDel" => "<button type='button' id='sDel' name='sDel' value style='border:none;background:none;'><img src='../images/del_hao.png'></button>"
        );
        $datatable = new datatable('resume',$_POST);
        $datatable->id = 'resume_id';
        $datatable->otherwhere = 'flag01 = "0"';
        $datatable->ExtendData($extendcolumn);
		$output = $datatable->show();
        echo json_encode($output);
		break;
	case 'add':
		parse_str(urldecode($_POST['data']), $data);
		parse_str(urldecode($_POST['ext']), $ext);
		parse_str(urldecode($_POST['rad']), $rad);

		 echo json_encode($data);exit;
		$trn = $resume->add(json_encode($data),json_encode($ext),json_encode($rad));
		echo json_encode($trn);
		// print_r($ext);
		// print_r($rad);
		// echo json_encode($data);
		break;
	case 'file':
		$resume_id = $_POST['resume_id'];
		$upload = new Upload(array('image/jpeg', 'image/png', 'image/gif'),array('jpeg', 'jpg', 'gif', 'png'),2097152, true,'../resume_file/'.$resume_id);
		$UploadDAO = new UploadDAO;

		$upload->callUploadFile();
		$upload_callback = $upload->getDestination();

		$DAO = $UploadDAO->add($resume_id,json_encode($upload_callback));

		break;
	case 'del':
		$resume_id = $_POST['resume_id'];
        $del = $resume->del($resume_id);
        echo json_encode($del);
		break;
	case 'view':
		$resume_id = $_POST['resume_id'];
		$resume_data = $resume->view_resume($resume_id);
		$UploadDAO = new UploadDAO;
		$imgarr = $UploadDAO->show($resume_id);
		$resultarray = array_merge(json_decode($resume_data,true),$imgarr);
		// json_decode($resume_data);
		echo json_encode($resultarray);
		break;
	case 'mobile_list':
		$data = $resume->ListAll();
		echo $data;
		break;
    case 'meslist':
        $extendcolumn = array(
            "sViewmes" => "<button type='button' id='sViewmes' name='sViewmes' value style='border:none;background:none;'><img src='../images/preview.png'></button>",
            "sDelmes" => "<button type='button' id='sDelmes' name='sDelmes' value style='border:none;background:none;'><img src='../images/del_hao.png'></button>"
        );
        $datatable = new findjob('resume',$_POST);
        $datatable->id = 'resume_id';
        $datatable->ExtendData($extendcolumn);
        $output = $datatable->show();
        echo json_encode($output);
        break;
    case 'mesdel':
        $trn = $_POST['trn'];
        $del = $resume->MesDel($trn);
        echo json_encode($del);
        break;
    case 'mesenter':
        # code...
        break;
	default:
		# code...
		break;
}
?>