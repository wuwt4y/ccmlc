DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `tmp_datatable`()
  BEGIN

    DROP TEMPORARY TABLE IF EXISTS datatable;

    CREATE TEMPORARY TABLE datatable(
      trn_no int(6) NOT NULL,
      resume_id varchar(7) NOT NULL,
      r_name varchar(30) NOT NULL,
      country varchar(30) NOT NULL,
      height int(5) NOT NULL,
      weight int(5) NOT NULL,
      birthday date NOT NULL,
      edu_level varchar(10) NOT NULL,
      edu_school varchar(50) NOT NULL,
      address varchar(250) NOT NULL,
      email varchar(80) NOT NULL,
      whatsapp varchar(30) NOT NULL,
      line_id varchar(30) NOT NULL,
      skill varchar(2000) NOT NULL,
      license varchar(2000) NOT NULL,
      special_job varchar(2000) NOT NULL,
      swt int(1) NOT NULL DEFAULT '0',
      flag01 int(1) NOT NULL DEFAULT '0',
      flag02 int(1) NOT NULL DEFAULT '0',
      c_date datetime NOT NULL,
      c_id varchar(25) NOT NULL,
      introname varchar(30) NOT NULL,
      introcontact varchar(500) NOT NULL
    );

    INSERT INTO datatable
      SELECT a.*,
        (SELECT b.val FROM resume_ext AS b WHERE b.type='introname' AND a.resume_id = b.resume_id),
        (SELECT b.val FROM resume_ext AS b WHERE b.type='introcontact' AND a.resume_id = b.resume_id)
      FROM resume AS a WHERE a.flag01 = '1';

  END$$
DELIMITER ;