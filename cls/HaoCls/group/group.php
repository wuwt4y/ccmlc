<?php
/**
 * Created by PhpStorm.
 * User: ShihHao
 * Date: 2018/1/26
 * Time: 下午 05:42
 */

namespace HaoCls\group;


use HaoCls\dao\MyPDO;

class group{
    private $add_sql = 'INSERT INTO group_mapping(group_name,c_date,c_id) VALUES(:group_name,:c_date,:c_id)';

    private $select_grouplist = 'SELECT group_id,group_name FROM group_mapping WHERE swt = "0"';

    private $_delsql = 'DELETE FROM group_mapping WHERE group_id = :group_id';

    private $_editsql = 'UPDATE group_mapping SET group_name = :group_name WHERE group_id = :group_id';

    public function add_group($group_name,$c_id)
    {
        try {
            $pdo = MyPDO::getInstance();
            $sh = $pdo->prepare($this->add_sql);
            $sh->bindValue(':group_name',$group_name);
            $sh->bindValue(':c_date',date('YmdHis'));
            $sh->bindValue(':c_id',$c_id);
            $sh->execute();
        } catch (PDOException $e) {
            err_log(__LINE__, $e->getCode(), $e->getMessage());
            echo $e -> getMessage();
        }
    }

    public function select_group_list()
    {
        try {
            $pdo = MyPDO::getInstance();
            $sh = $pdo->prepare($this->select_grouplist);
            $sh->execute();
            $stmt = $sh->fetchAll(\PDO::FETCH_ASSOC);
            return json_encode($stmt);
        } catch (PDOException $e) {
            err_log(__LINE__, $e->getCode(), $e->getMessage());
            echo $e -> getMessage();
        }
    }

    public function del_group($group_id)
    {
        try {
            $pdo = MyPDO::getInstance();
            $sh = $pdo->prepare($this->_delsql);
            $sh->bindValue(':group_id',$group_id);
            $sh->execute();
        } catch (PDOException $e) {
            err_log(__LINE__, $e->getCode(), $e->getMessage());
            echo $e -> getMessage();
        }
    }

    public function edit_group($group_id,$group_name)
    {
        try {
            $pdo = MyPDO::getInstance();
            $sh = $pdo->prepare($this->_editsql);
            $sh->bindValue(':group_id',$group_id);
            $sh->bindValue(':group_name',$group_name);
            $sh->execute();
        } catch (PDOException $e) {
            err_log(__LINE__, $e->getCode(), $e->getMessage());
            echo $e -> getMessage();
        }
    }
}