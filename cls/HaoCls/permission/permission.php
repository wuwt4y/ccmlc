<?php
/**
 * Created by PhpStorm.
 * User: ShihHao
 * Date: 2018/1/26
 * Time: 下午 05:32
 */
namespace HaoCls\permission;

class permission
{
	private $del_sql = "DELETE FROM group_permission WHERE group_type = :group_type AND group_id = :group_id";

	private $add_sql = "INSERT INTO group_permission(group_type,group_id,form_id,permission,c_date,c_id) VALUES(:group_type,:group_id,:form_id,:permission,:c_date,:c_id)";


	public function set($group_type,$group_id,$form_id,$permission)
	{
		try {
			$pdo = MyPDO::getInstance();
			$sh = $pdo->prepare($this->add_sql);
			$sh->bindValue(':group_type',$group_type);
			$sh->bindValue(':group_id',$group_id);
			$sh->bindValue(':form_id',$form_id);
			$sh->bindValue(':permission',$permission);
			$sh->bindValue(':c_date',date("YmdHis"));
			$sh->bindValue(':c_id',$_SESSION['usr_id']);
			$sh->execute();
		} catch (PDOException $e) {
			err_log(__LINE__, $e->getCode(), $e->getMessage());
			echo $e -> getMessage();
		}
	}

	public function unset($group_type,$group_id)
	{
		try {
			$pdo = MyPDO::getInstance();
			$sh = $pdo->prepare($this->del_sql);
			$sh->bindValue(':group_type',$group_type);
			$sh->bindValue(':group_id',$group_id);
			$sh->execute();
		} catch (PDOException $e) {
			err_log(__LINE__, $e->getCode(), $e->getMessage());
			echo $e -> getMessage();
		}
	}
}
?>