<?php
/**
 * Created by PhpStorm.
 * User: ShihHao
 * Date: 2018/1/26
 * Time: 下午 05:32
 */

namespace HaoCls\datatable;

class extendDatatable
{
    private $id;
    private $ExtenArr;
    public function get(array $arr,$id)
    {
        $this->id = $id;
        //利用value關鍵字 替換 value=id
        $this->ExtenArr = str_replace('value','value="'.$this->id.'"',$arr);
        return $this->ExtenArr;
    }
}