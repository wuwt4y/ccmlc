<?php
namespace HaoCls\datatable;
use HaoCls\dao\MyPDO;
use HaoCls\dao\MyQuery;
use \PDO;
class datatable
{
	//datatable post過來資料
	protected $post;

	//資料表名稱
	protected $TableName;

	//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
	protected $draw;

	//預設排序 index of the sorting column (0 index based - i.e. 0 is the first record)
	protected $orderByColumnIndex;

	//Get name of the sorting column from its index
	protected $orderBy;

	//ASC or DESC
	protected $orderType;

	//Paging first record indicator.
	protected $start;

	//Number of records that the table can display in the current draw
	protected $length;

	//global search values
	protected $searchvalue;

	//總資料筆數
	protected $iFilteredTotal;

	//過濾後紀錄資料筆數
	protected $recordsFiltered;

	//輸出sql語法
	protected $sql;

	//global search where條件語法
	protected $WhereSql;

	//footer search where條件語法
	protected $FootWhereSql;

	//輸出
	protected $stmt;

	//擴展資料欄位,例如更新 編輯 刪除...等
	protected $ExtendData;

	//dataTable 資料列表輸出
	protected $datainfo;

	//dataTable 資料輸出
	protected $output;

	//外部設定擴充column  value值
	public $id;

	//外部設定where條件 e.g. flag01 = '0';
	public $otherwhere;

    //$otherwhere 產生basic where語句
	protected $otherwhere1;

    //$otherwhere 產生有既有條件 AND語句
	protected $otherwhere2;



	public function __construct($TableName,$post)
	{
		$this->post = $post;
		$this->draw = $post['draw'];
		$this->orderByColumnIndex = $post['order'][0]['column'];
		$this->orderBy = $post['columns'][$this->orderByColumnIndex]['data'];
		$this->orderType = $post['order'][0]['dir'];
		$this->start = $post['start'];
		$this->length = $post['length'];
		$this->searchvalue = $post['search']['value'];
		$this->TableName = $TableName;
	}

	protected function GlobalWhereSql(): void
    {
		for($i = 0, $iMax = \count($this->post['columns']); $i < $iMax; $i++)
		{
	        $column = $this->post['columns'][$i]['data'];
	        //we get the name of each column using its index from POST request
	        $where[]="$column like '%".$this->searchvalue."%'";
        }
        $where = '('.implode(' OR ', $where).')';
        $this->WhereSql = $where;
	}

	protected function FootWhereSql(): void
    {
		for($i = 0, $iMax = \count($this->post['columns']); $i < $iMax; $i++)
		{
            if (empty($this->post['columns'][$i]['search']['value']))
            {
            	continue;
            }

            $column = $this->post['columns'][$i]['data'];
            $foowhere[]= "$column LIKE '%".$this->post['columns'][$i]['search']['value']."%' ";
        }
        if (isset($foowhere)) {
        	$foowhere = implode(' AND ', $foowhere);
        	$this->FootWhereSql = $foowhere;
        }
	}

	protected function Sql(): void
    {
    	if (!empty($this->otherwhere)) {
            $this->otherwhere1 = 'WHERE '.$this->otherwhere;
            $this->otherwhere2 = 'AND '.$this->otherwhere;
    	}
		if (empty($this->searchvalue) && empty($this->FootWhereSql)) {
			$this->sql = "SELECT * FROM $this->TableName $this->otherwhere1 ORDER BY $this->orderBy $this->orderType LIMIT $this->start,$this->length";
		}elseif (empty($this->searchvalue)) {
			$this->sql = "SELECT * FROM $this->TableName WHERE $this->FootWhereSql $this->otherwhere2 ORDER BY $this->orderBy $this->orderType LIMIT $this->start,$this->length";
		}elseif (empty($this->FootWhereSql)) {
			$this->sql = "SELECT * FROM $this->TableName WHERE $this->WhereSql $this->otherwhere2 ORDER BY $this->orderBy $this->orderType LIMIT $this->start,$this->length";
		}else{
			$this->sql = "SELECT * FROM $this->TableName WHERE $this->WhereSql AND $this->FootWhereSql $this->otherwhere2 ORDER BY $this->orderBy $this->orderType LIMIT $this->start,$this->length";
		}
	}

	protected function Filtered(): void
    {
		$this->iFilteredTotal = MyQuery::sqlCount("SELECT * FROM $this->TableName");
		$this->recordsFiltered = MyQuery::sqlCount(strstr($this->sql,'ORDER',true));
	}

	public function ExtendData(array $exten): void
    {
		$this->ExtendData = $exten;
	}

	protected function datainfo(): void
    {
		$i = 0;
		$pdo = MyPDO::getInstance();
		$sh = $pdo->prepare($this->sql);
		$sh->execute();
		while ($this->stmt = $sh->fetch(PDO::FETCH_ASSOC)) {
            $this->datainfo[$i]=$this->stmt;
            $extencolumn = new extendDatatable();
            $ExtColu = $extencolumn->get($this->ExtendData,$this->stmt[$this->id]);
            foreach ($ExtColu as $key => $val) {
				$this->datainfo[$i][]=$val;
			}
            $i++;
		}
	}

	protected function output(): void
    {
		if (isset($this->datainfo)==''){
            $this->datainfo='';
            }
            $this->output = array(
            'draw'   => (int)$this->draw,
            'recordsTotal' => $this->iFilteredTotal,
            'recordsFiltered' => $this->recordsFiltered,
            'data' => $this->datainfo
            );
	}

	public function show()
	{
		$this->GlobalWhereSql();
		$this->FootWhereSql();
		$this->Sql();
		$this->Filtered();
		$this->datainfo();
		$this->output();
		return $this->output;
	}
}

