<?php
/**
 * Created by PhpStorm.
 * User: ShihHao
 * Date: 2018/2/1
 * Time: 下午 03:54
 */

namespace HaoCls\datatable;


use HaoCls\dao\MyPDO;
use HaoCls\dao\MyQuery;

class findjob extends datatable
{
    //呼叫tmp_datatable 預存程序
    protected $sql_procedure = 'CALL tmp_datatable';

    protected function GlobalWhereSql(): void
    {
        for($i = 0, $iMax = \count($this->post['columns']); $i < $iMax; $i++)
        {
            $column = $this->post['columns'][$i]['data'];
            //we get the name of each column using its index from POST request
            $where[]="$column like '%".$this->searchvalue."%'";
        }
        $where = '('.implode(' OR ', $where).')';
        $this->WhereSql = $where;
    }

    protected function FootWhereSql(): void
    {
        for($i = 0, $iMax = \count($this->post['columns']); $i < $iMax; $i++)
        {
            if (empty($this->post['columns'][$i]['search']['value']))
            {
                continue;
            }

            $column = $this->post['columns'][$i]['data'];
            $foowhere[]= "$column LIKE '%".$this->post['columns'][$i]['search']['value']."%' ";
        }
        if (isset($foowhere)) {
            $foowhere = implode(' AND ', $foowhere);
            $this->FootWhereSql = $foowhere;
        }
    }

    protected function Sql(): void
    {
        if (empty($this->searchvalue) && empty($this->FootWhereSql)) {
            $this->sql = "SELECT * FROM datatable
            ORDER BY $this->orderBy $this->orderType 
            LIMIT $this->start,$this->length";
        }elseif (empty($this->searchvalue)) {
            $this->sql = "SELECT * FROM datatable
            WHERE $this->FootWhereSql ORDER BY $this->orderBy $this->orderType LIMIT $this->start,$this->length";
        }elseif (empty($this->FootWhereSql)) {
            $this->sql = "SELECT * FROM datatable
            WHERE $this->WhereSql ORDER BY $this->orderBy $this->orderType LIMIT $this->start,$this->length";
        }else{
            $this->sql = "SELECT * FROM datatable
            WHERE $this->WhereSql AND $this->FootWhereSql ORDER BY $this->orderBy $this->orderType LIMIT $this->start,$this->length";
        }
    }

    protected function Filtered(): void
    {
        $this->iFilteredTotal = MyQuery::sqlCount('SELECT * FROM datatable');
        $this->recordsFiltered = MyQuery::sqlCount(strstr($this->sql,'ORDER',true));
    }

    protected function CallProcedure()
    {
        /**
         * @tmp_datatable_procedure.sql
         */
        $pdo = MyPDO::getInstance();
        $sh = $pdo->prepare($this->sql_procedure);
        $sh->execute();
    }

    public function show()
    {
        $this->CallProcedure();
        $this->GlobalWhereSql();
        $this->FootWhereSql();
        $this->Sql();
        $this->Filtered();
        $this->datainfo();
        $this->output();
        return $this->output;
    }
}