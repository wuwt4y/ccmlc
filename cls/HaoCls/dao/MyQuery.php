<?php
/**
 * Created by PhpStorm.
 * User: ShihHao
 * Date: 2018/1/26
 * Time: 下午 05:47
 */

namespace HaoCls\dao;


class MyQuery{
    public static function sqlCount($sql)
    {
        $pdo = MyPDO::getInstance();
        $sh = $pdo->prepare($sql);
        $sh->execute();
        $count = $sh->rowCount();
        return $count;
    }
}