<?php
/**
 * Created by PhpStorm.
 * User: ShihHao
 * Date: 2018/1/26
 * Time: 下午 05:39
 */

namespace HaoCls\dao;

use \Dotenv\Dotenv;
use \PDO;
class MyPDO{
    public $db;
    private static $_instance;
    private function __construct()
    {
        $Dotenv = new Dotenv($_SERVER['DOCUMENT_ROOT']);
        $Dotenv->load();
        $dsn = 'mysql:dbname='.$_ENV['DB_DATABASE'].';host='.$_ENV['DB_HOST'].';port='.$_ENV['DB_PORT'];
        $this->db = new PDO($dsn, $_ENV['DB_USERNAME'], $_ENV['DB_PASSWORD']);
        $this->db->exec('SET NAMES "'.$_ENV['DB_CHARSET'].'"');
        $this->db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    }
    private function __clone(){ }
    public static function getInstance()
    {
        if (!isset(self::$_instance))
        {
            $object = __CLASS__;
            self::$_instance = new $object;
            self::$_instance->db;
        }
        return self::$_instance->db;
    }
}