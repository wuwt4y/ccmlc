<?php
namespace HaoCls\upload;

use HaoCls\dao\MyPDO;
use PDO;

class UploadDAO
{
	private $add = "INSERT INTO resume_file(resume_id,file_type,file_ori_name,file_uni_name,file_path,file_size,c_date,c_id) VALUES(:resume_id,:file_type,:file_ori_name,:file_uni_name,:file_path,:file_size,:c_date,:c_id)";

	private $show_img = "SELECT * FROM resume_file WHERE resume_id = :resume_id";

	public function add($resume_id,$input)
	{
		$pdo = MyPDO::getInstance();
		$jinput = json_decode($input,true);
		foreach ($jinput as $val) {
			$sh = $pdo->prepare($this->add);
			$sh->bindValue(':resume_id',$resume_id);
			$sh->bindValue(':file_type',$val['filetype']);
			$sh->bindValue(':file_ori_name',$val['oriname']);
			$sh->bindValue(':file_uni_name',$val['uniName']);
			$sh->bindValue(':file_path',$val['path']);
			$sh->bindValue(':file_size',$val['filesize']);
			$sh->bindValue(':c_date',date("YmdHis"));
			$sh->bindValue(':c_id',$_SESSION['usr_id']);
			$sh->execute();
		}
	}

	public function show($input)
	{
		$pdo = MyPDO::getInstance();
		$sh = $pdo->prepare($this->show_img);
		$sh->bindParam(':resume_id',$input);
		$sh->execute();
		$stmt = $sh->fetchAll(PDO::FETCH_ASSOC);
		$result = array('file' => $stmt);
		return $result;
	}
}
?>