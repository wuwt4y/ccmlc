<?php
namespace HaoCls\recruitment;

class findworker_mesDAO
{
	private $add_sql = "INSERT INTO findworker_mes(name,contact_way,introducer_name,introducer_contact,c_date,remote_ip) VALUES(:name,:contact_way,:introducer_name,:introducer_contact,:c_date,:remote_ip)";

	public function add($input)
	{
		try {
			$data = json_decode($input,true);
			$pdo = MyPDO::getInstance();
			$sh = $pdo->prepare($this->add_sql);
			$sh->bindParam(':name',$data['EmployerName']);
			$sh->bindParam(':contact_way',$data['EmployerContact']);
			$sh->bindParam(':introducer_name',$data['IntroEmpName']);
			$sh->bindParam(':introducer_contact',$data['IntroEmpContact']);
			$sh->bindValue(':c_date',date('YmdHis'));
			$sh->bindParam(':remote_ip',$_SERVER['REMOTE_ADDR']);
			$sh->execute();
			return 'success';
		} catch (PDOException $e) {
			err_log(__LINE__, $e->getCode(), $e->getMessage());
			echo $e -> getMessage();
		}
	}
}
?>