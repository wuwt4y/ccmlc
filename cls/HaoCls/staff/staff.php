<?php
/**
 * Created by PhpStorm.
 * User: ShihHao
 * Date: 2018/1/26
 * Time: 下午 05:49
 */

namespace HaoCls\staff;
use HaoCls\dao\MyPDO;

class staff{
    private $_sql_chk_id = "SELECT * FROM staff WHERE usr_id = :id";
    private $_sql_chk_passwd = "SELECT * FROM staff WHERE usr_id = :id AND usr_passwd = :passwd";
    private $_sql_insert = "INSERT INTO staff(group_id,usr_name,usr_id,usr_passwd,usr_email,usr_whatsapp,usr_skype,usr_line,usr_tel,creat_time,c_id) VALUES(:group_id,:usr_name,:usr_id,:usr_passwd,:usr_email,:usr_whatsapp,:usr_skype,:usr_line,:usr_tel,:date_time,:c_id)";
    private $_sql_sh = "SELECT * FROM staff WHERE trn_no = :trn_no";
    private $_sql_del = "DELETE FROM staff WHERE trn_no = :trn_no";
    private $_sql_edit = "UPDATE staff SET usr_name = :usr_name,usr_email = :usr_email,usr_whatsapp = :usr_whatsapp,usr_skype = :usr_skype,usr_line = :usr_line,usr_tel = :usr_tel,group_id = :group_id WHERE trn_no = :trn_no";
    private $_ModifyPasswd_sql = "UPDATE staff SET usr_passwd = :usr_passwd,passwd_uptime = :passwd_uptime WHERE usr_id = :usr_id";

    private function add_session($usr_id,$usr_name,$group_id)
    {
        $_SESSION['login_state'] = '1';
        $_SESSION['usr_id'] = $usr_id;
        $_SESSION['usr_name'] = $usr_name;
        $_SESSION['group_id'] = $group_id;
    }
    //驗證add staff 傳入資料
    private function _chk_add_staff_input($add_input)
    {
        $in_arr = json_decode($add_input,true);
        if (is_Id($in_arr['usr_id']) == false) {
            return my_errcode('450');
        }
        if (is_Id($in_arr['usr_passwd']) == false) {
            return my_errcode('452');
        }
        if (strlen($in_arr['usr_id']) <= 5 || strlen($in_arr['usr_id'])>=26 ) {
            return my_errcode('451');
        }
        if (strlen($in_arr['usr_passwd']) <= 5 || strlen($in_arr['usr_passwd'])>=26 ) {
            return my_errcode('453');
        }
        if ($in_arr['usr_passwd'] != $in_arr['usr_passwd2']) {
            return my_errcode('455');
        }
        if (!empty($in_arr['usr_email'])) {
            if (!filter_var($in_arr['usr_email'], FILTER_VALIDATE_EMAIL)) {
                return my_errcode('470');
            }
        }
        if (!empty($in_arr['usr_whatsapp'])) {
            if (is_Id($in_arr['usr_whatsapp']) == false) {
                return my_errcode('460');
            }
        }
        if (!empty($in_arr['usr_skype'])) {
            if (is_Id($in_arr['usr_skype']) == false) {
                return my_errcode('461');
            }
        }
        if (!empty($in_arr['usr_line'])) {
            if (is_Id($in_arr['usr_line']) == false) {
                return my_errcode('462');
            }
        }

        return $in_arr;
    }

    private function _chk_edit_staff_input($edit_input)
    {
        $in_arr = json_decode($edit_input,true);
        if (!empty($in_arr['usr_email'])) {
            if (!filter_var($in_arr['usr_email'], FILTER_VALIDATE_EMAIL)) {
                return my_errcode('470');
            }
        }
        if (!empty($in_arr['usr_whatsapp'])) {
            if (is_Id($in_arr['usr_whatsapp']) == false) {
                return my_errcode('460');
            }
        }
        if (!empty($in_arr['usr_skype'])) {
            if (is_Id($in_arr['usr_skype']) == false) {
                return my_errcode('461');
            }
        }
        if (!empty($in_arr['usr_line'])) {
            if (is_Id($in_arr['usr_line']) == false) {
                return my_errcode('462');
            }
        }
        return $in_arr;
    }

    public function chk_staff($id,$passwd)
    {
        try {
            $pdo = MyPDO::getInstance();
            $cou_id = $pdo->prepare($this->_sql_chk_id);
            $cou_id->bindValue(':id', $id);
            $cou_id->execute();
            $count_id = $cou_id->rowCount();
            if ($count_id == 1) {
                $cou_pd = $pdo->prepare($this->_sql_chk_passwd);
                $cou_pd->bindValue(':passwd', $passwd);
                $cou_pd->bindValue(':id', $id);
                $cou_pd->execute();
                $count_pd = $cou_pd->rowCount();
                $stmt = $cou_pd->fetch(\PDO::FETCH_ASSOC);
                if ($count_pd == 1) {
                    $this->add_session($stmt['usr_id'],$stmt['usr_name'],$stmt['group_id']);
                    return true;
                }else{
                    return my_errcode('421');
                }
            }else{
                return my_errcode('420');
            }
        } catch (PDOException $e) {
            err_log(__LINE__, $e->getCode(), $e->getMessage());
            echo $e -> getMessage();
        }
    }

    public function add_staff($add_input)
    {
        $chk_arr = $this->_chk_add_staff_input($add_input);
        if (!is_array($chk_arr)) {
            if (substr($chk_arr,0,1) == '4') {
                return $chk_arr;
            }
        }
        try {
            $pdo = MyPDO::getInstance();
            $sh = $pdo->prepare($this->_sql_insert);
            $sh->bindValue(':group_id',$chk_arr['group_id']);
            $sh->bindValue(':usr_name',$chk_arr['usr_name']);
            $sh->bindValue(':usr_id',$chk_arr['usr_id']);
            $sh->bindValue(':usr_passwd',$chk_arr['usr_passwd']);
            $sh->bindValue(':usr_email',$chk_arr['usr_email']);
            $sh->bindValue(':usr_whatsapp',$chk_arr['usr_whatsapp']);
            $sh->bindValue(':usr_skype',$chk_arr['usr_skype']);
            $sh->bindValue(':usr_line',$chk_arr['usr_line']);
            $sh->bindValue(':usr_tel',$chk_arr['usr_tel']);
            $sh->bindValue(':date_time',date("YmdHis"));
            $sh->bindValue(':c_id',$_SESSION['usr_id']);
            $sh->execute();
        } catch (PDOException $e) {
            err_log(__LINE__, $e->getCode(), $e->getMessage());
            echo $e -> getMessage();
        }
    }

    public function sh_staff($trn_no)
    {
        try {
            $pdo = MyPDO::getInstance();
            $sh = $pdo->prepare($this->_sql_sh);
            $sh->bindValue(':trn_no',$trn_no);
            $sh->execute();
            $stmt = $sh->fetchAll(\PDO::FETCH_ASSOC);
            return json_encode($stmt);
        } catch (PDOException $e) {
            err_log(__LINE__, $e->getCode(), $e->getMessage());
            echo $e -> getMessage();
        }
    }

    public function edit_staff($edit_input)
    {
        $chk_arr = $this->_chk_edit_staff_input($edit_input);
        if (!is_array($chk_arr)) {
            if (substr($chk_arr,0,1) == '4') {
                return $chk_arr;
            }
        }
        try {
            $pdo = MyPDO::getInstance();
            $sh = $pdo->prepare($this->_sql_edit);
            $sh->bindValue(':usr_name',$chk_arr['usr_name']);
            $sh->bindValue(':usr_email',$chk_arr['usr_email']);
            $sh->bindValue(':usr_whatsapp',$chk_arr['usr_whatsapp']);
            $sh->bindValue(':usr_skype',$chk_arr['usr_skype']);
            $sh->bindValue(':usr_line',$chk_arr['usr_line']);
            $sh->bindValue(':usr_tel',$chk_arr['usr_tel']);
            $sh->bindValue(':group_id',$chk_arr['egroup_id']);
            $sh->bindValue(':trn_no',$chk_arr['edit_trn']);
            $sh->execute();
        } catch (PDOException $e) {
            err_log(__LINE__, $e->getCode(), $e->getMessage());
            echo $e -> getMessage();
        }
    }

    public function del_staff($trn_no)
    {
        try {
            $pdo = MyPDO::getInstance();
            $sh = $pdo->prepare($this->_sql_del);
            $sh->bindValue(':trn_no',$trn_no);
            $sh->execute();
        } catch (PDOException $e) {
            err_log(__LINE__, $e->getCode(), $e->getMessage());
            echo $e -> getMessage();
        }
    }

    public function ModifyPasswd($input)
    {

    }
}