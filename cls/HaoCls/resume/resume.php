<?php
namespace  HaoCls\resume;
use HaoCls\dao\MPDO;
use HaoCls\dao\MyPDO;
use \PDO;
class resume
{
    /**
     * @var PDO
     */
	protected $add_resume = 'INSERT INTO resume(resume_id,r_name,country,height,weight,birthday,edu_level,edu_school,address,email,whatsapp,line_id,skill,license,special_job,c_date,c_id) VALUES (:resume_id,:r_name,:country,:height,:weight,:birthday,:edu_level,:edu_school,:address,:email,:whatsapp,:line_id,:skill,:license,:special_job,:c_date,:c_id)';

	protected $add_resume_ext = 'INSERT INTO resume_ext(resume_id,type,label,val) VALUES(:resume_id,:type,:label,:val)';

	protected $add_resume_radio = 'INSERT INTO resume_radio(resume_id,input_name,val) VALUES(:resume_id,:input_name,:val)';

	protected $auto_trn = 'SHOW TABLE STATUS WHERE name ="resume"';

    protected $sel_all = 'SELECT * FROM resume';

	protected $sel_resume = 'SELECT * FROM resume WHERE resume_id = :resume_id';

	protected $sel_resume_ext = 'SELECT * FROM resume_ext WHERE resume_id = :resume_id';

	protected $sel_resume_radio = 'SELECT * FROM resume_radio WHERE resume_id = :resume_id';

	protected $sel_click_count = 'SELECT type,count(type) c FROM resume_ext WHERE resume_id = :resume_id AND type in ("faname","wexp_country","tel_label","ftel_name") GROUP BY type';

	protected $del_resume = 'DELETE FROM resume WHERE resume_id = :resume_id';

	protected $DelMesResume = 'DELETE FROM resume WHERE resume_id = :resume_id AND flag01 = "1"';

	protected $trn;

	protected $resume_id;
//履歷表資料陣列
	protected $resume_info;

	protected function auto_trn()
	{
		$pdo = MyPDO::getInstance();
		$sh = $pdo->prepare($this->auto_trn);
		$sh->execute();
		$stmt = $sh->fetch(PDO::FETCH_ASSOC);
		$this->trn = 'R'.str_pad($stmt['Auto_increment'],6,'0',STR_PAD_LEFT);
	}

	protected function add_basic($input)
	{
			$pdo = MyPDO::getInstance();
			$data = json_decode($input,true);
			$sh = $pdo->prepare($this->add_resume);
			$sh->execute(array(
				':resume_id' => $this->trn,
				':r_name' => $data['r_name'],
				':country' => $data['country'],
				':height' => $data['height'],
				':weight' => $data['weight'],
				':birthday' => $data['birthday'],
				':edu_level' => $data['edu_level'],
				':edu_school' => $data['edu_school'],
				':address' => $data['address'],
				':email' => $data['email'],
				':whatsapp' => $data['whatsapp'],
				':line_id' => $data['line_id'],
				':skill' => $data['skill'],
				':license' =>  $data['license'],
				':special_job' => $data['special_job'],
				':c_date' => date("YmdHis"),
				':c_id' => $_SESSION['usr_id']
			));
			return true;
	}

	protected function add_ext($ext_input)
	{
		$pdo = MyPDO::getInstance();
		$ext = json_decode($ext_input,true);
		foreach ($ext as $key => $val) {
			if (empty($val)) {
				continue;
			}
			if (\is_array($val)) {
				foreach ($val as $dkey => $dval) {
					if (empty($dval)) {
						continue;
					}
					$sh = $pdo->prepare($this->add_resume_ext);
					$sh->execute(array(
						':resume_id' => $this->trn,
						':type' => $key,
						':label' => $dkey,
						':val' => $dval
					));
				}
				continue;
			}
			$sh = $pdo->prepare($this->add_resume_ext);
			$sh->execute(array(
				':resume_id' => $this->trn,
				':type' => $key,
				':label' => '0',
				':val' => $val
			));
		}
	}

	protected function add_radio($radio_input)
	{
		$pdo = MyPDO::getInstance();
		$radio = json_decode($radio_input,true);
		foreach ($radio as $key => $val) {
			$sh = $pdo->prepare($this->add_resume_radio);
			$sh->execute(array(
				':resume_id' => $this->trn,
				':input_name' => $key,
				':val' => $val
			));
		}
	}

	public function add($input,$ext_input,$radio_input)
	{
		try {
			$this->auto_trn();
			$this->add_basic($input);
			$this->add_ext($ext_input);
			$this->add_radio($radio_input);
			return array('success',$this->trn);
		} catch (PDOException $e) {
			err_log(__LINE__, $e->getCode(), $e->getMessage());
			echo $e -> getMessage();
		}
	}

	protected function Edit($resume_id,$input,$ext_input,$radio_input)
    {
        try {

            $pdo = MyPDO::getInstance();
            $pdo->beginTransaction();
            $sh = $pdo->prepare($this->del($resume_id));
            $sh->execute();
            $this->auto_trn();
            $this->add_basic($input);
            $this->add_ext($ext_input);
            $this->add_radio($radio_input);
        } catch (PDOException $e) {
            $pdo->rollBack();
            err_log(__LINE__, $e->getCode(), $e->getMessage());
            echo $e -> getMessage();
        }
    }

	public function ListAll()
	{
		try {
			$pdo = MyPDO::getInstance();
			$sh = $pdo->prepare($this->sel_all);
			$sh->execute();
			$stmt = $sh->fetchAll(PDO::FETCH_ASSOC);
			$result = json_encode($stmt);
			return $result;
		} catch (PDOException $e) {
			err_log(__LINE__, $e->getCode(), $e->getMessage());
			echo $e -> getMessage();
		}
	}

	protected function list_resume()
	{
			$pdo = MyPDO::getInstance();
			$sh = $pdo->prepare($this->sel_resume);
			$sh->bindParam(':resume_id',$this->resume_id);
			$sh->execute();
			$result = $sh->fetch(PDO::FETCH_ASSOC);
			return $result;
	}

	protected function list_resume_ext()
	{
			$pdo = MyPDO::getInstance();
			$sh = $pdo->prepare($this->sel_resume_ext);
			$sh->bindParam(':resume_id',$this->resume_id);
			$sh->execute();
			$result = $sh->fetchAll(PDO::FETCH_ASSOC);
			return $result;
	}

	protected function list_resume_radio()
	{
		$pdo = MyPDO::getInstance();
		$sh = $pdo->prepare($this->sel_resume_radio);
		$sh->bindParam(':resume_id',$this->resume_id);
		$sh->execute();
		$result = $sh->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	protected function ext_click_count()
	{
		$pdo = MyPDO::getInstance();
		$sh = $pdo->prepare($this->sel_click_count);
		$sh->bindParam(':resume_id',$this->resume_id);
		$sh->execute();
		$result = $sh->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function view_resume($input)
	{
		try {
			$this->resume_id = $input;
			$list_resume = $this->list_resume();
			$list_resume_ext = $this->list_resume_ext();
			$list_resume_radio = $this->list_resume_radio();
			$ext_click_count = $this->ext_click_count();
			$this->resume_info = array(
				'resume' => $list_resume,
				'resume_ext' => $list_resume_ext,
				'resume_radio' => $list_resume_radio,
				'ext_click' => $ext_click_count
				);
			// $array_r_e = array_merge($list_resume, $list_resume_ext);
			// $array_res = array_merge($array_r_e,$list_resume_radio);
			$result = json_encode($this->resume_info);
			return $result;
		} catch (PDOException $e) {
			err_log(__LINE__, $e->getCode(), $e->getMessage());
			echo $e -> getMessage().__LINE__;
		}
	}

	public function del($resume_id)
	{
		try {
			$pdo = MyPDO::getInstance();
			$sh = $pdo->prepare($this->del_resume);
			$sh->bindValue(':resume_id',$resume_id);
			$sh->execute();
			return 'success';
		} catch (PDOException $e) {
			err_log(__LINE__, $e->getCode(), $e->getMessage());
			echo $e -> getMessage().__LINE__;
		}
	}

	public function MesDel($resume_id)
    {
		try {
			$pdo = MyPDO::getInstance();
			$sh = $pdo->prepare($this->DelMesResume);
			$sh->bindValue(':resume_id',$resume_id);
			$sh->execute();
			return 'success';
		} catch (PDOException $e) {
			err_log(__LINE__, $e->getCode(), $e->getMessage());
			echo $e -> getMessage().__LINE__;
		}
	}
}
