<?php
namespace HaoCls\resume;
use HaoCls\dao\MyPDO;

class findjob_mesDAO extends resume
{
    protected $add_resume = 'INSERT INTO resume(resume_id,r_name,country,height,weight,birthday,edu_level,edu_school,address,email,whatsapp,line_id,skill,license,special_job,flag01,c_date,c_id) VALUES (:resume_id,:r_name,:country,:height,:weight,:birthday,:edu_level,:edu_school,:address,:email,:whatsapp,:line_id,:skill,:license,:special_job,:flag01,:c_date,:c_id)';

    protected function add_basic( $input): bool
    {
        $pdo = MyPDO::getInstance();
        $data = json_decode($input,true);
        $sh = $pdo->prepare($this->add_resume);
        $sh->execute(array(
            ':resume_id' => $this->trn,
            ':r_name' => $data['r_name'],
            ':country' => $data['country'],
            ':height' => $data['height'],
            ':weight' => $data['weight'],
            ':birthday' => $data['birthday'],
            ':edu_level' => $data['edu_level'],
            ':edu_school' => $data['edu_school'],
            ':address' => $data['address'],
            ':email' => $data['email'],
            ':whatsapp' => $data['whatsapp'],
            ':line_id' => $data['line_id'],
            ':skill' => $data['skill'],
            ':license' => $data['license'],
            ':special_job' => $data['special_job'],
            ':flag01' => '1',
            ':c_date' => date('YmdHis'),
            ':c_id' => $_SESSION['usr_id']
        ));
        return true;
    }
}
