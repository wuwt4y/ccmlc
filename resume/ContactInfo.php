<div class="resume_inner_title">
		<?=_('通訊資料')?>
</div>
<div id="p6_address">
	<?=_('地址：')?><label for="address"></label><input type="text" name="address" id="address">
</div>
<div class="telephone">
	<div id="p6_tel1">
		<div class="p6_tel1">
            <label>
                <input type="text" name="tel_label[0]" class="tel_label resume_ext" value="<?=_('電話')?>">
            </label>：<label for="tel0"></label><input type="text" name="tel[0]" id="tel0" class="resume_ext">
		</div>
	</div>
	<button class="btn btn-outline-primary btn-sm" id="add_tel_label"><img src="../images/add_hao.png"><?=_('新增電話')?></button>
</div>
<div id="p6_email">
    <?=_('E-Mail：')?><label for="email"></label><input type="text" name="email" id="email">
</div>
<div id="p6_whatapp">
	<?=_('Whats App：')?><label for="whatsapp"></label><input type="text" name="whatsapp" id="whatsapp">
</div>
<div id="p6_Lineid">
	<?=_('Line Id：')?><label for="line_id"></label><input type="text" name="line_id" id="line_id">
</div>
<div class="resume_inner_con_title">
		<?=_('家庭成員電話號碼')?>
</div>
<div id="p6_fami_tel">
	<div class="fami_tel_group">
		<div class="ftel_group_name">
			<?=_('姓名')?>
		</div>
		<div class="ftel_group_title">
			<?=_('稱謂')?>
		</div>
		<div class="ftel_group_tel">
			<?=_('電話')?>
		</div>
	</div>
	<div class="fami_tel_group">
		<div class="ftel_group_name">
            <label for="ftel_name_0"></label><input type="text" name="ftel_name[0]" id="ftel_name_0" class="resume_ext">
		</div>
		<div class="ftel_group_title">
            <label for="ftel_title_0"></label><input type="text" name="ftel_title[0]" id="ftel_title_0" class="resume_ext">
		</div>
		<div class="ftel_group_tel">
            <label for="ftel_tel_0"></label><input type="text" name="ftel_tel[0]" id="ftel_tel_0" class="resume_ext">
		</div>
		<div class="ftel_group_bu">
			<img src="../images/add_hao.png" id="add_ftel_group" class="resume_ext">
		</div>
	</div>
</div>
<div class="resume_inner_con_title">
		<?=_('最好朋友電話號碼')?>
</div>
<div id="p6_friend_tel">
	<?=_('姓名：')?><label for="friend_name_0"></label><input type="text" name="friend_name[0]" id="friend_name_0" class="resume_ext">
	<?=_('電話：')?><label for="friend_tel_0"></label><input type="text" name="friend_tel[0]" id="friend_tel_0" class="resume_ext">
</div>

