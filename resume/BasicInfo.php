<div class="resume_inner_title">
    <div class="r_title">
        <?=_('個人基本資料')?>
    </div>
</div>
<div class="p2_name">
    <?=_('姓名')?>：<label for="r_name"></label><input type="text" name="r_name" id="r_name">
</div>
<div id="p3_height">
    <?=_('身高：')?><label for="height"></label><input type="text" name="height" id="height">
</div>
<div id="p3_weight">
    <?=_('體重：')?><label for="weight"></label><input type="text" name="weight" id="weight">
</div>
<div id="p3_gender">
    <?=_('性別：')?>
    <?=_('男')?><label for="gender0"></label><input type="radio" name="gender" id="gender0" value="0" class="resume_radio">
    <?=_('女')?><label for="gender1"></label><input type="radio" name="gender" id="gender1" value="1" class="resume_radio">
</div>
<div id="p3_marry">
    <?=_('婚姻：')?>
    <?=_('已婚')?><label for="marry0"></label><input type="radio" name="marry" id="marry0" value="0" class="resume_radio">
    <?=_('未婚')?><label for="marry1"></label><input type="radio" name="marry" id="marry1" value="1" class="resume_radio">
</div>
<div id="p3_country">
    <?=_('國家：')?>
    <label for="country"></label><select name="country" id="country"></select>
</div>
<div id="p3_birthday">
    <?=_('生日：')?><label for="birthday"></label><input type="text" name="birthday" id="birthday">
</div>
<div id="p3_family_job">
    <div class="resume_inner_con_title"><?=_('家庭狀況')?></div>
    <div class="fgroup">
        <div class="fgroup_name">
            <?=_('姓名')?>
        </div>
        <div class="fgroup_title">
            <?=_('稱謂')?>
        </div>
        <div class="fgroup_age">
            <?=_('年齡')?>
        </div>
        <div class="fgroup_job">
            <?=_('職業')?>
        </div>
    </div>
    <div class="fgroup">
        <div class="fgroup_name">
            <label for="faname_0"></label><input type="text" name="faname[0]" id="faname_0" class="resume_ext">
        </div>
        <div class="fgroup_title">
            <label for="fatitle_0"></label><input type="text" name="fatitle[0]" id="fatitle_0" class="resume_ext">
        </div>
        <div class="fgroup_age">
            <label for="faage_0"></label><input type="text" name="faage[0]" id="faage_0" class="resume_ext">
        </div>
        <div class="fgroup_job">
            <label for="fajob_0"></label><input type="text" name="fajob[0]" id="fajob_0" class="resume_ext">
        </div>
        <div class="fgroup_bu">
            <img src="../images/add_hao.png" id="add_fa_group">
        </div>
    </div>
</div>
<div id="p3_edu">
    <div class="resume_inner_con_title">
        <?=_('個人學歷')?>
    </div>
    <div class="ed_sta">
        <label for="edu_level"><?=_('教育程度：')?></label><input type="text" name="edu_level" id="edu_level">
    </div>
    <div class="ed_sch">
        <label for="edu_school"><?=_('畢業學校：')?></label><input type="text" name="edu_school" id="edu_school">
    </div>
</div>
<div id="p3_skill">
    <div class="resume_inner_con_title">
        <?=_('專長')?>
    </div>
    <label for="skill"></label><textarea id="skill" name="skill" rows="5" cols="50" ></textarea>
</div>
<div id="p3_workexp">
    <div class="resume_inner_con_title">
        <?=_('工作經驗')?>
    </div>
    <div class="workexp_group">
        <div class="workexp_group_country">
            <?=_('工作國家')?>
        </div>
        <div class="workexp_group_star">
            <?=_('工作開始日期')?>
        </div>
        <div class="workexp_group_end">
            <?=_('工作結束日期')?>
        </div>
        <div class="workexp_group_class">
            <?=_('工作類別')?>
        </div>
        <div class="workexp_group_content">
            <?=_('工作內容')?>
        </div>
        <div class="workexp_group_quit">
            <?=_('離職原因')?>
        </div>
    </div>
    <div class="workexp_group">
        <div class="workexp_group_country">
            <label for="wexp_country_0"></label><input type="text" name="wexp_country[0]" id="wexp_country_0" class="resume_ext">
        </div>
        <div class="workexp_group_star">
            <label for="wexp_start_0"></label><input type="text" name="wexp_start[0]" id="wexp_start_0" class="resume_ext">
        </div>
        <div class="workexp_group_end">
            <label for="wexp_end_0"></label><input type="text" name="wexp_end[0]" id="wexp_end_0" class="resume_ext">
        </div>
        <div class="workexp_group_class">
            <label for="wexp_class_0"></label><input type="text" name="wexp_class[0]" id="wexp_class_0" class="resume_ext">
        </div>
        <div class="workexp_group_content">
            <label for="wexp_content_0"></label><input type="text" name="wexp_content[0]" id="wexp_content_0" class="resume_ext">
        </div>
        <div class="workexp_group_quit">
            <label for="wexp_quit_0"></label><input type="text" name="wexp_quit[0]" id="wexp_quit_0" class="resume_ext">
        </div>
        <div class="workexp_group_bu">
            <img src="../images/add_hao.png" id="add_workexp">
        </div>
    </div>
</div>