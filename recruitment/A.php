<div class="RecruitmentBasic">
	<div class="Rtitle">
			<?=_('應徵資訊：')?>
		</div>
	<div>
		<?=_('招聘單位：')?>
		<input type="text" name="RecruitmentUnit" id="RecruitmentUnit">
	</div>
	<div>
		<?=_('招聘單位官網：')?>
		<input type="text" name="RecruitmentUnitWebsite" id="RecruitmentUnitWebsite">
	</div>
	<div>
		<?=_('招聘工種：')?>
		<input type="text" name="RecruitmentType" id="RecruitmentType">
	</div>
	<div>
		<?=_('工作地點：')?>
		<input type="text" name="JobLocation" id="JobLocation">
	</div>
	<div>
		<?=_('職稱：')?>
		<input type="text" name="JobTitle" id="JobTitle">
	</div>
	<div>
		<?=_('薪資：')?>
		<input type="text" name="Wages" id="Wages">
	</div>
	<div>
		<?=_('福利：')?>
		<input type="text" name="Benefits" id="Benefits">
	</div>
	<div>
		<?=_('工作內容簡述：')?>
		<input type="text" name="JobContent" id="JobContent">
	</div>
</div>
