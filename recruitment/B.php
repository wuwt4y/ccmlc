<script type="text/javascript">
	$(function() {
		$( "input[type='radio']" ).checkboxradio();
		$('#SliderAgeRange').slider({
			range: true,
			min: 15,
			max: 70,
			values: [ 20, 40 ],
			slide: function( event, ui ) {
				$( "#AgeRange" ).text( ui.values[ 0 ] + " ~ " + ui.values[ 1 ] + "" );
			}
		});
		$('#SliderHeightRange').slider({
			range: true,
			min: 130,
			max: 200,
			values: [ 150, 175 ],
			slide: function( event, ui ) {
				$( "#HeightRange" ).text( ui.values[ 0 ] + "cm ~ " + ui.values[ 1 ] + "cm" );
			}
		});
		$('#SliderWeightRange').slider({
			range: true,
			min: 30,
			max: 150,
			values: [ 45, 75 ],
			slide: function( event, ui ) {
				$( "#WeightRange" ).text( ui.values[ 0 ] + "kg ~ " + ui.values[ 1 ] + "kg" );
			}
		});
	});
</script>
<div class="RecruitmentQualification">
	<div class="Rtitle">
		<?=_('應徵資格：')?>
	</div>
	<div>
		<?=_('年齡：')?>
		<div id="AgeRange">15 ~ 70</div>
		<div id="SliderAgeRange"></div>
	</div>
	<div>
		<?=_('性別：')?>
		<label for="male"><?=_('男性')?></label>
			<input type="radio" name="gender" id="male">
		<label for="female"><?=_('女性')?></label>
			<input type="radio" name="gender" id="female">
	</div>
	<div>
		<?=_('學歷：')?>
	</div>
	<div>
		<?=_('身高：')?>
		<div id="HeightRange">150cm ~ 175cm</div>
		<div id="SliderHeightRange"></div>
	</div>
	<div>
		<?=_('體重：')?>
		<div id="WeightRange">45kg ~ 75kg</div>
		<div id="SliderWeightRange"></div>
	</div>
	<div>
		<?=_('宗教：')?>
	</div>
	<div>
		<?=_('語言：')?>
	</div>
	<div>
		<?=_('電腦：')?>
	</div>
	<div>
		<?=_('駕照：')?>
	</div>
	<div>
		<?=_('備機車：')?>
	</div>
</div>