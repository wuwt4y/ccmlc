<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="author" content="oxxo.studio">
  <meta name="copyright" content="oxxo.studio">
  <title>Google 語音辨識 API - demo2</title>
  <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript">
      function speechRecognition(){
    if (!("webkitSpeechRecognition" in window)) {
      alert("本瀏覽器不支援語音辨識，請更換瀏覽器！(Chrome 25 版以上才支援語音辨識)");
    } else{
      window.recognition = new webkitSpeechRecognition();
      window.recognition.continuous = true;
      window.recognition.interimResults = true;
      window.recognition.lang = "cmn-Hant-TW";

      window.recognition.onstart = function() {
        console.log("Start recognize...");
      };

      window.recognition.onend = function() {
        window.recognition.start();
      };

      window.recognition.onresult = function(event,result) {
        result = {};
        result.resultLength = event.results.length-1;
        result.resultTranscript = event.results[result.resultLength][0].transcript;
        if(event.results[result.resultLength].isFinal===false){
          console.log(result.resultTranscript);
            document.getElementById("show").innerHTML = result.resultTranscript;
    if(result.resultTranscript.indexOf("哈囉")!==-1){
              alert('Hello!');
          }
    if(result.resultTranscript.indexOf("今天天氣")!==-1){
          $('div').css('display','none');
              $('#today_weather').css('display','inline');
              }
    if(result.resultTranscript.indexOf("紫外線")!==-1){
      $('div').css('display','none');
              $('#T_UV').css('display','inline');
          }
    if(result.resultTranscript.indexOf("一周天氣")!==-1){
      $('div').css('display','none');
              $('#week_weather').css('display','inline');
          }
    if(result.resultTranscript.indexOf("電影")!==-1){

          }
    if(result.resultTranscript.indexOf("開機")!==-1){
         $.ajax({
             url: 'wol.php',
             cache: false,
             dataType: 'html',
             type:'POST',
             data: { deltrn : 'deltrn',
             },
             error: function(xhr) {
             alert('Ajax request 發生錯誤');
             },//
             success: function () {
             // table.ajax.reload();
                 },
         });
          }
                }else if(event.results[result.resultLength].isFinal===true){
          console.log("final");
          console.log('---'+result.resultTranscript+'---');
        }
      };
      window.recognition.start();
    }
  }
    $(function() {
      $('body').on('click','#speechon',function(){
        speechRecognition();
        console.log(window.recognition);
      });
      $('body').on('click','#speechoff',function(){
        window.recognition.stop();
      });
    });
  </script>
  <style type="text/css">
  /*FcstBoxTable01*/
  table.FcstBoxTable01{ width:98%; /*border:#8297d6 1px solid;*/ padding:0; margin:0 auto 20px auto; border-collapse:collapse; }
  table.FcstBoxTable01 tr{}
  table.FcstBoxTable01 thead th{ font-weight:normal; background-color:#FFF187; text-align:center; /*border-bottom:#8297d6 1px solid; border-left:#8297d6 1px solid;*/padding:5px; color:#0086B3; font-weight:bold; font-size:13px;}
  table.FcstBoxTable01 tbody th{
  font-weight:normal; /*background-color:#edf0f9;*/ text-align:center;
  border-top:#8297d6 1px solid; /*border-left:#8297d6 1px solid;*/ padding:5px 5px 5px 10px; color:#0086B3; font-weight:bold;}
  table.FcstBoxTable01 td{border-top:#8297d6 1px solid; /*border-left:#8297d6 1px solid;*/ text-align:center;padding:5px 5px 5px 10px; font-family:Verdana, Arial, Helvetica, sans-serif;}
  table.FcstBoxTable01 td a{ color:#2f83cb; text-decoration:none;}
  table.FcstBoxTable01 td a:hover{ color:#2f83cb; text-decoration:underline;}
  </style>
</head>

<body>

  <h4>辨識的文字會顯示在下方：<h4>
  <h1 id="show"></h1>

  <button type="button" id="speechon">on</button>
  <button type="button" id="speechoff">off</button>
</body>

</html>
