<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<title></title>
	<!-- <script src="../js/jquery-3.2.1.min.js"></script> -->
	<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="../js/jquery-ui.min.js"></script>
	<script src = "../js/jquery.dataTables.min.js"></script>
	<script src = "../js/dataTables.scroller.min.js"></script>
    <script src="../js/jquery.mobile.datepicker.js"></script>
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
	<link rel = "stylesheet" href = "../css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../css/jquery.mobile.datepicker.css">

	<script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
	<script type="text/javascript">

		$(function() {
            // $('#birthday').datepicker({changeYear: true, changeMonth: true, currentText: "Now", dateFormat:"yy-mm-dd", showButtonPanel: true, yearRange : "-90:+60",maxDate : '+0'});
            $.ajax({
                url: '../processing/country_processing.php?mode=list',
                type: 'POST',
                dataType: 'json'
            })
                .done(function(e) {
                    console.log(e);
                    for (var i = 0; i < e.length; i++) {
                        $('#country').append($("<option></option>").attr("value", e[i]['0']).text(e[i]['1']));
                    }
                })
                .fail(function() {
                    console.log("error");
                });
			$.ajax({
				url: '../processing/resume_processing.php?mode=mobile_list',
				type: 'POST',
				dataType: 'json'
			})
			.done(function(e) {
				console.log(e);
			})
			.fail(function() {
				console.log("error");
			});
		resume_table = $('#Table_resume').DataTable( {
		        "oLanguage": {"sUrl": "../js/zh_TW.txt"},
		        "processing": true,
		        "serverSide": true,
		        "ajax": {
		            "url": "../processing/resume_processing.php?mode=list",
		            "type": "POST"
		        },
		        "columns":
		         [
		            { "data": "r_name","width": "15%" },
		            { "data": "birthday","width": "15%" },
		            { "data": "0" ,"width": "2%","orderable":false,"searchable":false}
		        ],
		        "order": [[0, 'desc']]
    		});
			$('body').on('click','#sView',function(){
				var val = $(this).val();
				 location.href = "resume.php?"+val;
			});

			$('#findjob_mes_bu').click(function(){
                var data = $('#findjob_mes_form input,textarea,select').not('.resume_ext,.resume_radio').serialize();
                var ext = $('#findjob_mes_form .resume_ext').serialize();
                var rad = $('#findjob_mes_form .resume_radio').serialize();
				$.ajax({
					url: './ajax/findjob_mes.php',
					type: 'POST',
					dataType: 'json',
                    data: {data: data,ext: ext,rad: rad}
				})
				.done(function(e) {
				    console.log(e);
					if (e == 'success') {
						alert('<?=_('訊息已送出,我們將有專人為你服務')?>');
						location.href = "find_worker.php";
					}
				})
				.fail(function() {
					console.log("error");
				});
			});
			$('#findworker_mes_bu').click(function(){
				$.ajax({
					url: './ajax/findworker_mes.php',
					type: 'POST',
					dataType: 'json',
					data: {data: $('#findworker_mes_form input').serialize()},
				})
				.done(function(e) {
					if (e == 'success') {
						alert('<?=_('訊息已送出,我們將有專人為你服務')?>');
						location.href = "find_worker.php";
					}
				})
				.fail(function() {
					console.log("error");
				});
			});
		});
	</script>
	<style type="text/css">
		.ui-page {
    		background:hsl(133, 44%, 87%);
		}
		body{
			background-image: url('../images/app_home.jpg');
			font-family: Microsoft JhengHei;
		}
		table{
			/*width: 100%;*/
		}
		#head{
			/*height:70px;*/
			background-color: hsl(152, 27%, 58%);
			/*color: #FFBD00;*/
			/*font-size: 24px;*/
		}
	</style>
</head>
<body>
<!-- <div id="home" data-role="page" style="style:background:#000;"> -->

<div id="home" data-role="page" style="background:url(../images/app_home.jpg); background-size:100%">
	<div data-role="header" id="head">
	  <a href="#" class="ui-btn ui-btn-left ui-corner-all ui-shadow ui-icon-home ui-btn-icon-left ui-btn-b" ><?=_('首頁')?></a>
	  <h1><?=_('尋找工人')?></h1>
	</div>
  <div role="main" class="ui-content">
<!-- 	<a href="#NewPage" data-transition="pop">
  	<button class="ui-btn" id="findwork"><?=_('尋找工人')?></button>
	</a> -->
	<a href="#NewPage" data-role="button" data-icon="search"><?=_('尋找工人')?></a>
	<a href="#findjob" data-role="button" data-icon="search"><?=_('我要找工作')?></a>
	<a href="#findworker" data-role="button" data-icon="search"><?=_('我要招工')?></a>
<!-- 	<div class="ui-field-contain">
		<label for="#text_id"></label>
		<input type="text" name="" id="#input_account" placeholder="<?=_('請輸入帳號')?>">
	</div>

	<div class="ui-field-contain">
		<label for="#text_id"></label>
		<input type="password" name="" id="#input_passwd" placeholder="<?=_('請輸入密碼')?>">
	</div>

	<button class="ui-btn" id="login"><?=_('登入')?></button>
	<button class="ui-btn" id="register"><?=_('註冊')?></button> -->

  </div>
  <!-- <div data-role="footer" data-position="fixed">
    <h3>Footer</h3>
  </div> -->
</div>

<div id="NewPage" data-role="page" style="background:hsl(214, 93%, 88%);">
	<div data-role="header">
	  <a href="#home" class="ui-btn ui-btn-left ui-corner-all ui-shadow ui-icon-home ui-btn-icon-left"><?=_('首頁')?></a>
	  <h1><?=_('尋找工人')?></h1>
	</div>
 	<table id="Table_resume" class="display" cellspacing="0" width="100%" height="400px">
        <thead>
            <tr>
              <th><?=_('姓名')?></th>
              <th><?=_('生日')?></th>
              <th><?=_('瀏覽')?></th>
            </tr>
        </thead>
    </table>
	<div data-role="footer" data-position="fixed">
			<a href="#home" data-role="button" class="ui-btn ui-icon-back ui-btn-icon-left" data-mini="true">
				<?=_('返回')?>
			</a>
	</div>
</div>

<div id="findjob" data-role="page" style="background:hsl(214, 93%, 88%);">
	<div data-role="header">
	  <a href="#home" class="ui-btn ui-btn-left ui-corner-all ui-shadow ui-icon-home ui-btn-icon-left"><?=_('首頁')?></a>
	  <h1><?=_('找工作')?></h1>
	</div>
	  <div data-role="content">
          <div data-role="fieldcontain" id="findjob_mes_form">
              <label for="r_name"><?= _('姓名：') ?></label>
              <input type="text" name="r_name" id="r_name">
              <label for="introname"><?= _('介紹人姓名：') ?></label>
              <input type="text" name="introname" id="introname" class="resume_ext">
              <label for="introcontact"><?= _('介紹人聯絡方式：') ?></label>
              <input type="text" name="introcontact" id="introcontact" class="resume_ext">
              <?= _('性別：') ?>
              <fieldset data-role="controlgroup" data-type="horizontal">
                  <input type="radio" name="gender" id="gender0" data-mini="true" value="0" class="resume_radio">
                  <label for="gender0"><?= _('男') ?></label>
                  <input type="radio" name="gender" id="gender1" data-mini="true" value="1" class="resume_radio">
                  <label for="gender1"><?= _('女') ?></label>
              </fieldset>
              <label for="country"><?= _('國家：') ?></label>
                  <select name="country" id="country">        
                  </select>
              <label for="height"><?= _('身高：') ?></label><input type="number" name="height" id="height">
              <label for="weight"><?= _('體重：') ?></label><input type="number" name="weight" id="weight">
              <label for="birthday"><?= _('生日：') ?></label><input type="date" data-role="date" data-inline="true"
                                                                  name="birthday" id="birthday">
              <label for="tel0"><?= _('電話1：') ?></label>
              <input type="text" name="tel[0]" id="tel0" class="resume_ext">
              <label for="tel[1]"><?= _('電話2：') ?></label>
              <input type="text" name="tel[1]" id="tel[1]" class="resume_ext">
              <label for="address"><?= _('地址：') ?></label>
              <input type="text" name="address" id="address">
              <label for="whatsapp"><?= _('WhatsApp ID：') ?></label>
              <input type="text" name="whatsapp" id="whatsapp">
              <label for="line_id"><?= _('Line ID：') ?></label>
              <input type="text" name="line_id" id="line_id">
              <label for="email"><?= _('E-Mail：') ?></label>
              <input type="text" name="email" id="email">
              <label for="edu_level"><?=_('教育程度')?></label>
              <input type="text" name="edu_level" id="edu_level">
              <label for="edu_school"><?=_('畢業學校：')?></label>
              <input type="text" name="edu_school" id="edu_school">
              <label for="skill"><?=_('專長')?></label>
              <textarea id="skill" name="skill" rows="5" cols="50" ></textarea>
              <label for="license"><?=_('個人專長和證明')?></label>
              <textarea id="license" name="license" rows="5" cols="50"></textarea>
              <label for="special_job"><?=_('特殊工作經驗')?></label>
              <textarea id="special_job" name="special_job" rows="5" cols="50"></textarea>
          </div>
          <input type="button" data-inline="true" value="提交" id="findjob_mes_bu">
	  </div>
	<div data-role="footer" data-position="fixed">
			<a href="#home" data-role="button" class="ui-btn ui-icon-back ui-btn-icon-left" data-mini="true">
				<?=_('返回')?>
			</a>
	</div>
</div>

<div id="findworker" data-role="page" style="background:hsl(214, 93%, 88%);">
	<div data-role="header">
	  <a href="#home" class="ui-btn ui-btn-left ui-corner-all ui-shadow ui-icon-home ui-btn-icon-left"><?=_('首頁')?></a>
	  <h1><?=_('聘工')?></h1>
	</div>
 	<div data-role="content">
	      <div data-role="fieldcontain" id="findworker_mes_form">
	        <label for="EmployerName"><?=_('雇主姓名：')?></label>
	        <input type="text" name="EmployerName" id="EmployerName">
	        <label for="EmployerContact"><?=_('聯絡方式：')?></label>
	        <input type="text" name="EmployerContact" id="EmployerContact">
	        <label for="IntroEmpName"><?=_('介紹人姓名：')?></label>
	        <input type="text" name="IntroEmpName" id="IntroEmpName">
	        <label for="IntroEmpContact"><?=_('介紹人聯絡方式：')?></label>
	        <input type="text" name="IntroEmpContact" id="IntroEmpContact">
	      </div>
	      <input type="button" data-inline="true" value="提交" id="findworker_mes_bu">
	</div>
	<div data-role="footer" data-position="fixed">
			<a href="#home" data-role="button" class="ui-btn ui-icon-back ui-btn-icon-left" data-mini="true">
				<?=_('返回')?>
			</a>
	</div>
</div>

<div id="success" data-role="page" style="background:hsl(214, 93%, 88%);">
	<div data-role="header">
	  <a href="#home" class="ui-btn ui-btn-left ui-corner-all ui-shadow ui-icon-home ui-btn-icon-left"><?=_('首頁')?></a>

	 <?=_('訊息已送出,我們將有專人為你服務')?>
	<div data-role="footer" data-position="fixed">
			<a href="#home" data-role="button" class="ui-btn ui-icon-back ui-btn-icon-left" data-mini="true">
				<?=_('返回')?>
			</a>
	</div>
</div>
</body>
</html>