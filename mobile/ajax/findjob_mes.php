<?php
session_start();
include_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/include/function.php';
date_default_timezone_set('Asia/Taipei');

use HaoCls\resume\findjob_mesDAO;
$findjob_mesDAO = new findjob_mesDAO;

parse_str(urldecode($_POST['data']), $data);
parse_str(urldecode($_POST['ext']), $ext);
parse_str(urldecode($_POST['rad']), $rad);

$trn = $findjob_mesDAO->add(json_encode($data),json_encode($ext),json_encode($rad));
echo json_encode($trn);
//$result = $findjob_mesDAO->add($jsondata);
//echo json_encode($result);

