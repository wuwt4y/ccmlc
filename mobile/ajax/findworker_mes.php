<?php
session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/include/function.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/cls/class_cls.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/cls/findworker_mesDAO.php');
date_default_timezone_set('Asia/Taipei');

// $fullname = $_POST['fullname'];
// $contact = $_POST['contact'];
// $introname = $_POST['introname'];
// $introcontact = $_POST['introcontact'];
$findworker_mesDAO = new findworker_mesDAO;

parse_str($_POST['data'],$data);

$jsondata = json_encode($data);
$result = $findworker_mesDAO->add($jsondata);
echo json_encode($result);
?>
