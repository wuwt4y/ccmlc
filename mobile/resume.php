<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<title></title>
	<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
	<script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
	<script type="text/javascript">
		$(function() {
			resume_id = location.search.replace('?','');
			$.ajax({
				url: '../processing/resume_processing.php?mode=view',
				type: 'POST',
				dataType: 'json',
				data: {resume_id: resume_id}
			})
			.done(function(res) {
				console.log(res);
				for (var i = 0; i < res.ext_click.length; i++) {
					var cclick;

					if (res.ext_click[i].type == "faname") {
						for (var j = 0; j < res.ext_click[i].c; j++) {
							var tagf = '<div class="tt1" id="fatitle'+j+'"></div><div class="tt2"><?=_('姓名：')?><span id="faname'+j+'"></span></br><?=_('年齡：')?><span id="faage'+j+'"></span></br><?=_('職業：')?><span id="fajob'+j+'"></span></span>';
							$('#fami').append(tagf);
						}
					 }

					// if (res.ext_click[i].type == "ftel_name") {cclick = 'add_ftel_group' }
					if (res.ext_click[i].type == "tel_label") {
						for (var j = 0; j < res.ext_click[i].c; j++) {
							var tag = "<div><span id='tel_label"+j+"'></span>：<span id='tel"+j+"'></span></div>";
							$('#contact').append(tag);
						}
					}
					if (res.ext_click[i].type == "wexp_country") {
						for (var j = 0; j < res.ext_click[i].c; j++) {
							var tagf = '<div class="tt1" id="wexp_class'+j+'"></div><div class="tt2"><?=_('工作地點：')?><span id="wexp_country'+j+'"></span></br><?=_('開始日期：')?><span id="wexp_start'+j+'"></span></br><?=_('結束日期：')?><span id="wexp_end'+j+'"></span></br><?=_('工作內容：')?><span id="wexp_content'+j+'"></span></br><?=_('離職原因：')?><span id="wexp_quit'+j+'"></span></span>';
							$('#exp').append(tagf);
						}
					}

				}
				$.each(res.resume, function(key, value){
				    $('#'+key).append(value);
				});
				for (var i = 0; i < res.resume_ext.length; i++) {
					var inp_name = res.resume_ext[i].type+'['+res.resume_ext[i].label+']';
					$('input[name="'+inp_name+'"]').val(res.resume_ext[i].val);
					$('#'+res.resume_ext[i].type+res.resume_ext[i].label).text(res.resume_ext[i].val);
				}
				for (var i = 0; i < res.resume_radio.length; i++) {
					$('label[for="'+res.resume_radio[i].input_name+res.resume_radio[i].val+'"]').removeClass('ui-radio-off').addClass('ui-radio-on').addClass('ui-btn-active');
					$('input[name="'+res.resume_radio[i].input_name+'"][value="'+res.resume_radio[i].val+'"]').prop('checked',true);
				}
				for (var i = 0; i < res.file.length; i++) {
					// res.file[i]
					$('#resume_photo').append('<img src="'+res.file[i].file_path+'">');
				}
			})
			.fail(function() {
				console.log("error");
			});

			$('#back_home').click(function(){
				location.href = "find_worker.php";
			});

			$('#back_page').click(function(){
				location.href = "find_worker.php";
			});

			$('body').on('click','.tt1',function(){
				$(this).next('.tt2').toggle('normal');
			});
		});
	</script>
	<style type="text/css">
		.ui-page {
    		background:hsl(133, 44%, 87%);
		}
		body{
			background-image: url('../images/app_home.jpg');
			font-family: Microsoft JhengHei !important;
		}
		#head{
			background-color: hsl(152, 27%, 58%);
			color: FFBD00;
		}
		#sch_ti{
			height: 26px;
			background-color: hsl(228, 14%, 82%);
			border-radius: 10px;
			font-size: 20px;
			font-family: Microsoft JhengHei;
			text-align: center;
		}
		.tt2{
			display: none;
		}
		.tt1{
			background-color: hsl(100, 54%, 81%);
			height: 30px;
			border-radius: 8px;
			margin-top: 8px;
		}
		.ui-content{
			padding-top: 0;
			padding-bottom: 0;
		}
	</style>
</head>
<body>
<div id="home" data-role="page" style="background:url(../images/app_home.jpg); background-size:100%;">
	<div data-role="header" id="head">
	  <a href="" class="ui-btn ui-btn-left ui-corner-all ui-shadow ui-icon-home ui-btn-icon-left ui-btn-b" id="back_home">
	  	<?=_('首頁')?>
	  </a>
	  <h1><?=_('履歷表')?></h1>
	</div>
	<div data-role="content">
	    <div data-role="collapsible" data-collapsed="false" data-inset="false">
	      <h1><?=_('基本資料')?></h1>
	      <div id="r_name"><?=_('姓名：')?></div>
	      <div id=""><?=_('性別：')?>
	      	<fieldset data-role="controlgroup" data-type="horizontal" >
		        <input type="radio" name="gender" id="gender0" data-mini="true" value="0" disabled>
		        <label for="gender0"><?=_('男')?></label>
		        <input type="radio" name="gender" id="gender1" data-mini="true" value="1" disabled>
		        <label for="gender1"><?=_('女')?></label>
		    </fieldset>
	      </div>
	      <div id="height"><?=_('身高：')?></div>
	      <div id="weight"><?=_('體重：')?></div>
	      <div id="birthday"><?=_('生日：')?></div>
	      <div id="marry"><?=_('婚姻：')?>
	      	<fieldset data-role="controlgroup" data-type="horizontal" >
		        <input type="radio" name="marry" id="marry0" data-mini="true" value="0" disabled>
		        <label for="marry0"><?=_('已婚')?></label>
		        <input type="radio" name="marry" id="marry1" data-mini="true" value="1" disabled>
		        <label for="marry1"><?=_('未婚')?></label>
		    </fieldset>
	      </div>
	      <div id="sch_ti">
	      	<?=_('個人學歷')?>
	      </div>
	      <div id="edu_level"><?=_('教育程度：')?></div>
	      <div id="edu_school"><?=_('畢業學校：')?></div>
	    </div>
    </div>
    <div data-role="content">
	    <div data-role="collapsible" data-collapsed="true" data-inset="false">
	      <h1><?=_('家庭狀況')?></h1>
	      <div id="fami"></div>
			<!-- <div class="tt1">父親
				<div class="tt2">
					<div>XXX</div>
					<div>ff</div>
					<div>ff</div>
				</div>
			</div> -->
	    </div>
    </div>
    <div data-role="content">
	    <div data-role="collapsible" data-collapsed="true" data-inset="false">
	      <h1><?=_('通訊資料')?></h1>
	      <div id="address"><?=_('地址：')?></div>
	      <div id="whatsapp"><?=_('WhatsApp：')?></div>
	      <div id="line_id"><?=_('Line：')?></div>
	      <div id="contact"></div>
	    </div>
    </div>

    <div data-role="content">
	    <div data-role="collapsible" data-collapsed="true" data-inset="false">
	      <h1><?=_('工作經驗')?></h1>
	      <div id="exp"></div>
	    </div>
    </div>

    <div data-role="content">
	    <div data-role="collapsible" data-collapsed="true" data-inset="false">
	      <h1><?=_('專長')?></h1>
	      <div id="skill"></div>
	    </div>
    </div>

    <div data-role="content">
	    <div data-role="collapsible" data-collapsed="true" data-inset="false">
	      <h1><?=_('專長及證照')?></h1>
	      <div id="license"></div>
	    </div>
    </div>

    <div data-role="content">
	    <div data-role="collapsible" data-collapsed="true" data-inset="false">
	      <h1><?=_('特殊工作經驗')?></h1>
	      <div id="special_job"></div>
	    </div>
    </div>

  <div data-role="footer" data-position="fixed">
			<a href="#" data-role="button" class="ui-btn ui-icon-back ui-btn-icon-left" data-mini="true" id="back_page">
				<?=_('返回')?>
			</a>
	</div>
</div>
</body>
</html>