<?php
session_start();
include "../tracy.php";
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<script src = "../js/jquery-3.2.1.min.js"></script>
	<script src = "../js/jquery-ui.min.js"></script>
	<script src = "../js/bootstrap.min.js"></script>
	<script src = "../js/jquery.fancybox.min.js"></script>
	<script src = "../js/jquery.dataTables.min.js"></script>
	<script src = "../js/dataTables.scroller.min.js"></script>
	<script src="../js/dropzone.min.js"></script>
	<link rel = "stylesheet" href = "../css/jquery.fancybox.min.css">
	<link rel = "stylesheet" href = "../css/bootstrap.min.css">
	<link rel = "stylesheet" href = "../css/style.css">
	<link rel = "stylesheet" href = "../css/jquery-ui.min.css">
	<link rel = "stylesheet" href = "../css/jquery.dataTables.min.css">
	<link rel = "stylesheet" href = "../css/dropzone.min.css">
	<script type="text/javascript">
		$(document).ready(function() {
			$('#staff_manage').click(function(){
				$.post("../control_page/staff_manage.php",function(data){
					$("#slide_iframe").empty();
					$("#slide_iframe").html(data);
				});

				$.ajax({
					url: '../processing/group_manage_processing.php?mode=select_group_list',
					type: 'POST',
					dataType: 'json',
					data: {param1: 'value1'},
				})
				.done(function(e) {
					for (var i = 0; i < e.length; i++) {
						 $("#select_group").append($("<option></option>").attr("value", e[i].group_id).text(e[i].group_name));
						 $("#eselect_group").append($("<option></option>").attr("value", e[i].group_id).text(e[i].group_name));
					}
				})
				.fail(function() {
					console.log("error");
				});
			});
			$('#group_manage').click(function(){
				$("#slide_iframe").empty();
				$("#slide_iframe").load('../control_page/group_manage.php');
			});
			$('#personal_manage').click(function(){
				$.post("../control_page/personal_info.php",function(data){
					$("#slide_iframe").empty();
					$("#slide_iframe").html(data);
				});
			});
			$('#permission_manage').click(function(){
				$.post("../control_page/permission_set.php",function(data){
					$("#slide_iframe").empty();
					$("#slide_iframe").html(data);
				});
			});
			$('#resume_manage').click(function(){
				$("#slide_iframe").load('../control_page/resume.php');
			});
			$('#HireWorkers_manage').click(function(){
				$("#slide_iframe").load('../control_page/hiretable.php');
			});
			$('#AdminManage').click(function(){
				$('.AdminManageDetail').toggle('normal');
			});
			$('body').on('click','#add_group',function(){
				$.fancybox.open({
					src:'#add_group_box',
					type:'inline',
					opts : {
						afterClose : function(){
							$('#a_group_name').val('');
						},
		                afterShow : function( instance, current ) {
		                // console.info( 'done!' );
		                }
           			 }
				});
			});
			$('body').on('click','#add_staff_bu',function(){
				$.fancybox.open({
					src:'#add_staff_box',
					type:'inline',
					opts : {
						afterClose: function( instance, current ) {

						},
		                afterShow : function( instance, current ) {
		                // console.info( instance );
		                }
           			 }
				});
			});
			$('body').on('click','#save_staff',function(){
				$.ajax({
					url: '../processing/staff_processing.php?mode=add_staff',
					type: 'POST',
					dataType: 'html',
					data: {data: $('#add_staff_box input,select[name="group_id"]').serialize()},
				})
				.done(function(e) {
					console.log(e);
					$('#err_add_staff').text(e).show();
					table.ajax.reload();
				})
				.fail(function(e) {
					console.log(e);
				});
			});
			$('body').on('click','div[id^=page_flow_l_]',function(){
				if (typeof($(this).attr('selected')) == 'undefined') {
					console.log('no');
					$(this).attr('selected',true);
					$(this).css('background-color','hsl(41, 47%, 77%)');
				}else{
					console.log('yes');
					$(this).removeAttr('selected');
					$(this).css('background-color','');
				}
			});
			$('body').on('click','div[id^=page_flow_r_]',function(){
				if ($(this).attr('selected') == 'selected') {
					$(this).removeAttr('selected');
					$(this).css('background-color','');
				}else{
					$(this).attr('selected',true);
					$(this).css('background-color','hsl(41, 47%, 77%)');
				}
			});
			$('body').on('click','#add_resume_bu',function(){
				$.fancybox.open({
					src:'../p.php',
					type:'iframe',
					opts : {
						afterClose: function( instance, current ) {

						},
		                afterShow : function( instance, current ) {
		                // console.info( instance );
		                }
           			 }
				});
			});
			$('body').on('click','#ViewResumeMessageBu',function(){
				$.fancybox.open({
					src:'#TableResumeMes_wrapper',
					type:'inline',
					opts : {
						afterClose: function( instance, current ) {

						},
		                afterShow : function( instance, current ) {
		                // console.info( instance );
		                }
           			 }
				});
			});
		});
		function onSelectGroup() {
		     var sel_text = $("#select_group").find("option:selected").text();
		     var sel_val = $("#select_group").val();
		     console.log(sel_text); //可以取得 select.text
		     console.log(sel_val);  //可以取得 select.value
   			}
   		function onSelectPermissionGroup() {
		     var sel_text = $("#group_permission").find("option:selected").text();
		     var sel_val = $("#group_permission").val();
		     console.log(sel_text); //可以取得 select.text
		     console.log(sel_val);  //可以取得 select.value
   			}
	</script>
</head>
<body>
	<?php
include_once('slide_toolbar.php');
include_once('slide_content.php');
	?>
</body>
</html>