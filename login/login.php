<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<script src="../js/jquery-3.2.1.min.js"></script>
	<script src="../js/jquery-ui.min.js"></script>
	<link rel = "stylesheet" href = "../css/style.css">
	<link rel = "stylesheet" href = "../css/jquery-ui.min.css">
	<script type="text/javascript">
		$(document).ready(function() {
			$('#slogin').click(function(){
				$.ajax({
					url: '../processing/login_processing.php?mode=login',
					type: 'POST',
					dataType: 'json',
					data: {
 						id:$('#userid').val(),
 						passwd:$('#password').val()
					},
				})
				.done(function(e) {
					if (e.state.substr(0,1) == '4') {
						console.log(e);
						$('.error_message').text(e.state);
					}else{
						document.location.href="control.php";
					}
				})
				.fail(function(e) {
					console.log(e);
				});
			});
		});
	</script>
</head>
<body>
<div class="content">
   <div class="login">
      <div class="news_top">
         <?=_('後臺管理系統')?>
      </div>
      <div class="news_top_line"></div>
<div id="login_content" class="login_content">
   <div class="login_id">
      <input type="text" name="Username" id="userid" size="15" maxlength="15" placeholder="<?=_('帳號')?>">
   </div>
   <div class="login_password">
      <input type="password" name="Password" id="password" size="15" maxlength="15" placeholder="<?=_('密碼')?>">
   </div>
   <div class="login_send">
      <input type="submit" value="<?=_('登入')?>" id="slogin">
   </div>
   <div class="error_message"></div>
</div>
</body>
</html>