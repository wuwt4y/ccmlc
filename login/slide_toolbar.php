<div id="slide_toolbar">
    <img src="../images/menu.png" alt="" id="menuicon">
	<div class="wellcome">
		<div class="wellname">
			<?=$_SESSION['usr_name']?>,<?=_('你好!')?>
		</div>
		<button id="logout" class="btn btn-sm btn-primary"><?=_('登出')?></button>
	</div>
	<div class="contentmenu">
		<div class="AdminManage">
			<button id="AdminManage" class="btn btn-sm btn-info"><?=_('系統管理與設定')?></button>
		</div>

		<div class="AdminManageDetail">
			<div class="staff_manage">
				<button id="staff_manage" class="btn btn-sm btn-danger"><?=_('員工管理')?></button>
			</div>
			<div class="group_manage">
				<button id="group_manage" class="btn btn-sm btn-danger"><?=_('群組管理')?></button>
			</div>
			<div class="permission_manage">
				<button id="permission_manage" class="btn btn-sm btn-danger"><?=_('權限管理')?></button>
			</div>
			<div class="ResumeSetting">
				<button id="ResumeSetting" class="btn btn-sm btn-danger"><?=_('履歷表設定')?></button>
			</div>
			<div class="RecruitmentTableSetting">
				<button id="RecruitmentTableSetting" class="btn btn-sm btn-danger"><?=_('聘工表設定')?></button>
			</div>
		</div>

		<div class="personal_manage">
			<button id="personal_manage" class="btn btn-sm btn-info"><?=_('個人基本資料設定')?></button>
		</div>
		<div class="resume_manage">
			<button id="resume_manage" class="btn btn-sm btn-info"><?=_('履歷表')?></button>
		</div>
		<div class="HireWorkers_manage">
			<button id="HireWorkers_manage" class="btn btn-sm btn-info"><?=_('聘工表')?></button>
		</div>
	</div>
</div>