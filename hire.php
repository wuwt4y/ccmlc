<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<script src = "js/jquery-3.2.1.min.js"></script>
	<script src = "js/jquery-ui.min.js"></script>
	<script src = "js/bootstrap.min.js"></script>
	<script src = "js/jquery.fancybox.min.js"></script>
	<script src = "js/jquery.dataTables.min.js"></script>
	<script src = "js/dataTables.scroller.min.js"></script>
	<script src="js/dropzone.min.js"></script>
	<link rel = "stylesheet" href = "css/jquery.fancybox.min.css">
	<link rel = "stylesheet" href = "css/bootstrap.min.css">
	<link rel = "stylesheet" href = "css/style.css">
	<link rel = "stylesheet" href = "css/jquery-ui.min.css">
	<link rel = "stylesheet" href = "css/jquery.dataTables.min.css">
	<link rel = "stylesheet" href = "css/dropzone.min.css">
</head>
<body>
<?php
include 'recruitment/A.php';
include 'recruitment/B.php';
include 'recruitment/C.php';
?>
</body>
</html>