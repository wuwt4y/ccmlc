<?php
//include "tracy.php";
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<script src = "js/jquery-3.2.1.min.js"></script>
	<script src = "js/jquery-ui.min.js"></script>
	<script src = "js/bootstrap.min.js"></script>
	<script src = "js/jquery.fancybox.min.js"></script>
	<script src = "js/jquery.dataTables.min.js"></script>
	<script src = "js/dataTables.scroller.min.js"></script>
	<script src="js/dropzone.min.js"></script>
	<link rel = "stylesheet" href = "css/jquery.fancybox.min.css">
	<link rel = "stylesheet" href = "css/bootstrap.min.css">
	<link rel = "stylesheet" href = "css/style.css">
	<link rel = "stylesheet" href = "css/jquery-ui.min.css">
	<link rel = "stylesheet" href = "css/jquery.dataTables.min.css">
	<link rel = "stylesheet" href = "css/dropzone.min.css">
	<script type="text/javascript">
        function ListPreResume(id) {
            var resume_id = id;
            $.ajax({
                url: 'processing/resume_processing.php?mode=view',
                type: 'POST',
                dataType: 'json',
                data: {resume_id: resume_id}
            })
                .done(function(res) {
                    console.log(res);
                    for (var i = 0; i < res.ext_click.length; i++) {
                        var cclick;
                        if (res.ext_click[i].type == "faname") {cclick = 'add_fa_group' }
                        if (res.ext_click[i].type == "ftel_name") {cclick = 'add_ftel_group' }
                        if (res.ext_click[i].type == "tel_label") {cclick = 'add_tel_label' }
                        if (res.ext_click[i].type == "wexp_country") {cclick = 'add_workexp' }
                        for (var j = 0; j < res.ext_click[i].c -1; j++) {
                            $('#'+cclick).click();
                        }
                    }
                    $.each(res.resume, function(key, value){
                        $('#'+key).val(value);
                    });
                    for (var i = 0; i < res.resume_ext.length; i++) {
                        var inp_name = res.resume_ext[i].type+'['+res.resume_ext[i].label+']';
                        $('input[name="'+inp_name+'"]').val(res.resume_ext[i].val);
                    }
                    for (var i = 0; i < res.resume_radio.length; i++) {
                        $('input[name="'+res.resume_radio[i].input_name+'"][value="'+res.resume_radio[i].val+'"]').prop('checked',true);
                    }
                    for (var i = 0; i < res.file.length; i++) {
                        // res.file[i]
                        $('#resume_photo').append('<img src="'+res.file[i].file_path+'">');
                    }
                })
                .fail(function() {
                    console.log("error");
                });
        }
		Dropzone.autoDiscover = false;
		$(document).ready(function() {
			//拖拉上傳檔案
			var myDropzone = new Dropzone("#myDropzone",{
			  dictDefaultMessage: "把檔案拉到這裡就可以上傳",
			  dictCancelUpload: "刪除",
			  dictCancelUploadConfirmation: "刪除",
			  dictRemoveFile: "刪除",
			  url: "processing/resume_processing.php?mode=file",
			  addRemoveLinks: true,
			  autoProcessQueue: false,
			  uploadMultiple: true,
			  parallelUploads: 10,
			  maxFiles: 10,
			  init: function() {
			    var myDropzone = this;
			    // this.on("sending", function(file, xhr, formData){
	      //           formData.append("fpos", 777)
	      //       }),
			    this.on("sendingmultiple", function() {
			      // Gets triggered when the form is actually being sent.
			      // Hide the success button or the complete form.
			    });
			    this.on("successmultiple", function(files, response) {
			      // Gets triggered when the files have successfully been sent.
			      // Redirect user or notify of success.
			      console.log(response);
			      this.removeAllFiles(true);
			    });
			    this.on("errormultiple", function(files, response) {
			      // Gets triggered when there was an error sending the files.
			      // Maybe show form again, and notify user of error
			      console.log(response);
			    });
			  }
			});
            var resume_id = location.hash.replace('#', '');
			$.ajax({
			            url: 'processing/country_processing.php?mode=list',
			            type: 'POST',
			            dataType: 'json'
			        })
			        .done(function(e) {
			            console.log(e);
                        for (var i = 0; i < e.length; i++) {
                            $('#country').append($("<option></option>").attr("value", e[i]['0']).text(e[i]['1']));
                        }
                        ListPreResume(resume_id);
			        })
			        .fail(function() {
			            console.log("error");
			        });

			$('#birthday').datepicker({changeYear: true, changeMonth: true, currentText: "Now", dateFormat:"yy-mm-dd", showButtonPanel: true, yearRange : "-90:+60",maxDate : '+0'});
			$('#add_fa_group').click(function(){
				var fg_num = $('.fgroup').length -1 ;
				var contain = '<div class="fgroup"><div class="fgroup_name"><input type="text" name="faname['+fg_num+']" id="faname_'+fg_num+'" class="resume_ext"></div><div class="fgroup_title"><input type="text" name="fatitle['+fg_num+']" id="fatitle_'+fg_num+'" class="resume_ext"></div><div class="fgroup_age"><input type="text" name="faage['+fg_num+']" id="faage_'+fg_num+'" class="resume_ext"></div><div class="fgroup_job"><input type="text" name="fajob['+fg_num+']" id="fajob_'+fg_num+'" class="resume_ext"></div><div class="fgroup_bu"><img src="../images/del_hao.png" class="del_fgroup"></div></div>';
				$('#p3_family_job').append(contain);
			});
			$('[id^="wexp_start"],[id^="wexp_end"]').datepicker({changeYear: true, changeMonth: true, currentText: "Now", dateFormat:"yy-mm-dd", showButtonPanel: true, yearRange : "-90:+60",maxDate : '+0'});
			$('#add_workexp').click(function(){
				var wexpg_num = $('.workexp_group').length -1;
				var contain = '<div class="workexp_group"><div class="workexp_group_country"><input type="text" name="wexp_country['+wexpg_num+']" id="wexp_country_'+wexpg_num+'" class="resume_ext"></div><div class="workexp_group_star" class="resume_ext"><input type="text" name="wexp_start['+wexpg_num+']" id="wexp_start_'+wexpg_num+'" class="resume_ext"></div><div class="workexp_group_end"><input type="text" name="wexp_end['+wexpg_num+']" id="wexp_end_'+wexpg_num+'" class="resume_ext"></div><div class="workexp_group_class" class="resume_ext"><input type="text" name="wexp_class['+wexpg_num+']" id="wexp_class_'+wexpg_num+'" class="resume_ext"></div><div class="workexp_group_content"><input type="text" name="wexp_content['+wexpg_num+']" id="wexp_content_'+wexpg_num+'" class="resume_ext"></div><div class="workexp_group_quit"><input type="text" name="wexp_quit['+wexpg_num+']" id="wexp_quit_'+wexpg_num+'" class="resume_ext"></div><div class="workexp_group_bu"><img src="../images/del_hao.png" class="del_workexp"></div></div>';
				$('#p3_workexp').append(contain);
				$('[id^="wexp_start"],[id^="wexp_end"]').datepicker({changeYear: true, changeMonth: true, currentText: "Now", dateFormat:"yy-mm-dd", showButtonPanel: true, yearRange : "-90:+60",maxDate : '+0'});
			});
			$('body').on('click','.del_fgroup,.del_workexp,.del_ftel',function(){
				$(this).parents().eq(1).remove();
			});
			$('#add_ftel_group').click(function(){
				var ftel_num = $('.fami_tel_group').length -1 ;
				var contain = '<div class="fami_tel_group"><div class="ftel_group_name"><input type="text" name="ftel_name['+ftel_num+']" id="ftel_name'+ftel_num+'" class="resume_ext"></div><div class="ftel_group_title"><input type="text" name="ftel_title['+ftel_num+']" id="ftel_title'+ftel_num+'" class="resume_ext"></div><div class="ftel_group_tel"><input type="text" name="ftel_tel['+ftel_num+']" id="ftel_tel'+ftel_num+'" class="resume_ext"></div><div class="ftel_group_bu"><img src="../images/del_hao.png" class="del_ftel"></div></div>';
				$('#p6_fami_tel').append(contain);
			});
			$('#add_tel_label').click(function(){
				var tel_label_num = $('.p6_tel1').length;
				var contain = '<div class="p6_tel1"><input type="text" name="tel_label['+tel_label_num+']" class="tel_label resume_ext" value="<?=_('電話')?>">：<input type="text" name="tel['+tel_label_num+']" id="tel'+tel_label_num+'" class="resume_ext"/><img src="../images/del_hao.png" class="del_tel"></div>';
				$('#p6_tel1').append(contain);
			});
			$('body').on('click','.del_tel',function(){
				$(this).parents().eq(0).remove();
			});

			$('body').on('click','#save_resume_bu',function(){
                var data = $('.resume_content input,textarea,select').not('.resume_ext,.resume_radio').serialize();
                var ext = $('.resume_ext').serialize();
                var rad = $('.resume_radio').serialize();
				// console.log(ext);
				$.ajax({
					url: '../processing/resume_processing.php?mode=add',
					type: 'POST',
					dataType: 'json',
					data: {data: data,ext: ext,rad: rad}
				})
				.done(function(e) {
					if (e[0] == 'success') {
						myDropzone.on("sending", function(file, xhr, formData){
			                formData.append("resume_id", e[1])
			            });
						myDropzone.processQueue();//觸發檔案上傳
					}
                    // resume_table.ajax.reload();
					console.log(e);
				})
				.fail(function() {
					console.log("error");
				});
			});
		});
	</script>
</head>
<body>
	<div class="resume_content">
<?php
include 'resume/BasicInfo.php';
include 'resume/Skill.php';
include 'resume/Photo.php';
include 'resume/ContactInfo.php';
?>
	</div>
	<button type="submit" id="save_resume_bu"><?=_('儲存')?></button>
</body>
</html>